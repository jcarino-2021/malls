<?php include('./includes/constants.php'); ?>
<!-- Constantes de la aplicación -->
<?php include('./includes/header.php'); ?>

<div id="app" v-cloak>
    <!-- Slider -->
    <div class="container-fluid d-flex align-items-center justify-content-center" style="background-image: url('./assets/img/eventos/header.png'); height: 550px; background-size: cover; background-position: center;">
        <h2 class="text-white special-font">Eventos</h2>
    </div>
    <div class="container my-5">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Eventos actuales</h2>
                
            </div>
        </div>
    </div>
    <div v-for="(upcomingEvent, index) in upcomingEvents">

        <div v-if="(index + 1)%2 > 0" class="container-fluid my-5 left-image-right-content">
            <div class="row my-4">
                <div class="col-md-6 px-0">
                    <img :src="upcomingEvent.image" :alt="upcomingEvent.name">
                </div>
                <div class="col-md-6 d-flex flex-column align-items-center justify-content-center mt-sm-5">
                    <div class="content-side">

                        <h2 class="font-light">{{upcomingEvent.name}}</h2>
                        <p class="text-muted font-light">{{upcomingEvent.description}}</p>
                    </div>

                </div>
            </div>
        </div>
        <div v-if="(index + 1)%2 === 0" class="container-fluid my-10 right-image-left-content ">
            <div class="row my-4">
                <div class="col-md-6 d-flex flex-column align-items-center justify-content-center order-1 order-md-1 order-sm-2 mt-sm-5">
                    <div class="content-side">
                        <h2 class="font-light">{{upcomingEvent.name}}</h2>
                        <p class="text-muted font-light">{{upcomingEvent.description}}</p>
                    </div>
                </div>
                <div class="col-md-6 px-0 order-1 order-sm-1  order-md-2">
                    <img :src="upcomingEvent.image" :alt="upcomingEvent.name">
                </div>
            </div>
        </div>
    </div>

</div>


<?php include('./includes/footer-includes.php'); ?>
<!-- Load js used in this page -->
<script type="module" src="./assets/js/scripts/events.js"></script>
<?php include('./includes/footer.php'); ?>