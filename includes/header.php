<!doctype html>
<html lang="en">

<head>
    <?php include('seo-tags.php'); ?> <!-- Configuración de SEO TAGS como OG o Twitter -->
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   
    <?php include('header-includes.php'); ?> <!-- Archivos .css que necesita el sitio -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-145714157-12"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-145714157-12');
    </script>
</head>

<body>

<?php 
    if (!isset($hideMenu)) {
        include('menu.php');
    }
?>