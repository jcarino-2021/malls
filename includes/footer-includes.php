    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo $rootDir; ?>/assets/js/libraries/jquery-3.4.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <!-- NOW UI Kit JavaScript -->
    <script src="<?php echo $rootDir; ?>/assets/js/now-ui-kit.js"></script>


    <!-- Plugins -->
    <script src="<?php echo $rootDir; ?>/assets/js/plugins/moment.min.js"></script>
    <script src="<?php echo $rootDir; ?>/assets/js/plugins/bootstrap-switch.js"></script>
    <script src="<?php echo $rootDir; ?>/assets/js/plugins/bootstrap-tagsinput.js"></script>
    <script src="<?php echo $rootDir; ?>/assets/js/plugins/bootstrap-selectpicker.js"></script>
    <script src="<?php echo $rootDir; ?>/assets/js/plugins/jasny-bootstrap.min.js"></script>
    <script src="<?php echo $rootDir; ?>/assets/js/plugins/nouislider.min.js"></script>
    <script src="<?php echo $rootDir; ?>/assets/js/plugins/bootstrap-datetimepicker.js"></script>

    <!-- Owl carousel -->
    <link rel="stylesheet" href="<?php echo $rootDir; ?>/assets/js/plugins/owl-carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $rootDir; ?>/assets/js/plugins/owl-carousel/assets/owl.theme.default.min.css">
    <script src="<?php echo $rootDir; ?>/assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
     <!-- Sweetalert -->
    
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
 
    

    <!-- Noty notification library -->
    <link href="<?php echo $rootDir; ?>/assets/js/plugins/noty/noty.css" rel="stylesheet">
    <link href="<?php echo $rootDir; ?>/assets/js/plugins/noty/theme/nest.css" rel="stylesheet">
    <script src="<?php echo $rootDir; ?>/assets/js/plugins/noty/noty.js" type="text/javascript"></script>
    <script type="module" src="<?php echo $rootDir; ?>/assets/js/utilities/notifications.js"></script>

    <!-- development version, includes helpful console warnings -->
    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
    <script src="<?php echo $rootDir; ?>/assets/js/globals.js"></script>

    <?php include('top-floating-btn.php'); ?>


   









    <!-- Vue plugins -->
    <!-- Loading -->
    <script src="https://cdn.jsdelivr.net/npm/vue-loading-overlay@3"></script>
    <link href="https://cdn.jsdelivr.net/npm/vue-loading-overlay@3/dist/vue-loading.css" rel="stylesheet">

    <!-- Form validators plugin -->
    <script src="https://unpkg.com/vuelidate@0.7.6/dist/vuelidate.min.js"></script>
    <script src="https://unpkg.com/vuelidate@0.7.6/dist/validators.min.js"></script>
    <!-- Init the plugin and component-->
    <script>
        Vue.use(window.vuelidate.default)
        Vue.use(VueLoading);
        Vue.component('loading', VueLoading)
    </script>