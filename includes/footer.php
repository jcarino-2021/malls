<footer id="app-footer" class="bg-primary mt-5 px-0" v-cloak>
    <div class="container-fluid">
        <div class="row justify-content-center text-center">
            <div class="col-md-12 my-1 mb-5">
                <h2 class="text-white special-font">Suscríbete</h3>
                    <form action="" id="newsletter-form" novalidate @submit.prevent="submitNewsletter">
                        <div class="input-group mb-3" style="max-width: 600px; width:100%;text-align: center;margin-left: auto;margin-right: auto;">
                            <input type="email" id="newsletter-input" class="form-control" placeholder="Ingresa tu correo electrónico" aria-label="Newsletter" aria-describedby="newsletter-btn" :class="{'is-invalid': $v.newsletter_email.$error}" v-model.trim="$v.newsletter_email.$model" required>
                            <form-feedback-component :is-valid="false" :message="'Debes ingresar un correo electrónico'"></form-feedback-component>    
                        </div>
                        
                        <button type="submit" class="btn btn-secondary font-light" style="min-width:120px; padding-top: 5px; padding-bottom: 5px;">Enviar </button>
                        <p class="text-info font-light">{{newsLetterRequestResponseText}}</p>
                    </form>
            </div>
            <div class="col-md-6 col-lg-3 col-sm-12" v-if="footerData.map">
                <div class="text-left mb-3">
                    <img src="<?php echo $rootDir . $logoFooterUrl; ?>" alt="">
                </div>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3860.1567264984606!2d-90.48395734860318!3d14.647043289722777!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8589a2858b795577%3A0x148c20c396c75cd8!2sCentro%20Comercial%20Portales%20Guatemala!5e0!3m2!1ses!2sgt!4v1633037226670!5m2!1ses!2sgt" width="100%" height="250" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

            </div>
            <div class="col-md-6 col-lg-3 col-sm-12 pt-5">
                <table class="opening-times-table mx-auto mb-3" v-for="schedule in footerData.schedule">
                    <tr>
                        <th class="text-uppercase">
                            <p style="font-size: 1rem; " class="special-font">{{schedule.title}}</p>
                        </th>
                    </tr>
                    <tr v-for="openingTime in schedule.opening_hours">
                        <td>
                            {{openingTime.days}}
                        </td>
                        <td>{{openingTime.schedule}}</td>
                    </tr>
                </table>
            </div>
            <div class="col-md-6 col-lg-3 col-sm-12 pt-5">
                <p class="text-uppercase text-white mb-0 special-font">Spectrum app</p>
                <div>
                    <a href="https://apps.apple.com/gt/app/spectrum-app/id1474578786"> <img src="<?php echo $rootUrl;?>/assets/img/icons/appstore.svg" width="50" alt=""></a>
                    <a href="https://play.google.com/store/apps/details?id=spectrum.frictionless.com"> <img src="<?php echo $rootUrl;?>/assets/img/icons/playstore.svg" width="70" alt=""></a>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 col-sm-12 pt-5" id="footer-sitemap">
                <p class="text-uppercase text-white special-font">Sitemap</p>
                <div class="row">
                    <div class="col-6 text-left">
                        <dl>
                            <dt><a href="<?php echo $rootUrl;?>/eventos.php">Happening Now</a></dt>
                            <dt><a href="<?php echo $rootUrl;?>/directorio.php">Directorio</a></dt>
                            <dt><a href="<?php echo $rootUrl;?>/blog.php">Trending Now</a></dt>
                            <dt><a href="https://cinepolis.com.gt/cartelera/guatemala-guatemala/cinepolis-oakland-mall">Cinépolis</a></dt>
                            <dt><a href="<?php echo $rootUrl;?>/beneficios.php">Beneficios</a></dt>
                        </dl>
                    </div>
                    <div class="col-6 text-left">
                        <dl>
                            <dt><a href="<?php echo $rootUrl;?>/el-circo.php">El Circo</a></dt>

                            <dt><a href="<?php echo $rootUrl;?>/megasale/index.php">Megasale</a></dt>
                            <dt><a href="<?php echo $rootUrl;?>/servicios.php">Servicios</a></dt>
                        </dl>
                    </div>
                </div>

            </div>
            <div class="col-md-10 my-5" style="border-top: 1px solid white;padding: 10px 5px;">
                <div class="row">
                    <div class="col-md-6 text-left">
                        <p class="text-white mb-0 font-light" style="font-size:0.8rem; margin-top:12px; ">Copyright ©2020 All rights reserved | Powered by Lumation Services LLC</p>
                    </div>
                    <div class="col-md-6 text-right">
                        <a v-for="socialNetwork in footerData.social_networks" class="mx-1" :href="socialNetwork.url">
                            <img :src="socialNetwork.logo" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


</footer>
<!-- Load js used in this page -->
<script type="module" src="<?php echo $rootDir;?>/assets/js/scripts/footer.js"></script>

</body>

</html>