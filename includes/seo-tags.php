
<?php
//SET TAGS ACCORDING TO API CALL OR DEFAULT
if(!isset($seoTitle)) {
    $seoTitle = $pageName;
    $seoDescription = $pageDescription;
    $seoImageUrl = $pageLogo;
}

?>




<title><?php echo $seoTitle ?></title>
<meta name="description" content="<?php echo $seoDescription ?>"/>
<meta property="og:title" content="<?php echo $seoTitle ?>" />
<meta property="og:description" content="<?php echo $seoDescription ?>" />
<meta property="og:image" content="<?php echo $seoImageUrl ?>" />