<?php
/* rootUrl comes from constants */
$uri = $_SERVER['REQUEST_URI'];



$activeOption = '';
$aboutUsMenu = '';
$directoryMenu = '';
$eventsMenu = '';
$blogMenu = '';
$benefitsMenu = '';
$elCircoMenu = '';
$rooftopMenu = '';
$servicesMenu = '';
$megaSaleMenu = '';

switch ($uri) {
  case $rootDir . '/quienes-somos.php':
    $aboutUsMenu = 'active';
    break;
  case $rootDir . '/directorio.php':
    $directoryMenu = 'active';
    break;
  case $rootDir . '/eventos.php':
    $eventsMenu = 'active';
    break;
  case $rootDir . '/blog.php':
    $blogMenu = 'active';
    break;
  case $rootDir . '/beneficios.php':
    $benefitsMenu = 'active';
    break;
  case $rootDir . '/el-circo.php':
    $elCircoMenu = 'active';
    break;
  case $rootDir . '/rooftop.php':
    $rooftopMenu = 'active';
    break;
  case $rootDir . '/servicios.php':
    $servicesMenu = 'active';
    break;
  case $rootDir . '/megasale/index.php':
    $megaSaleMenu = 'active';
    break;
}

$menuExtraClass = 'home-menu-transparent home-menu';

if ($uri == $rootDir . '/' || $uri == $rootDir . '/index.php') {
  $menuExtraClass = 'home-menu-transparent home-menu';
}

?>


<div id="app-menu" v-cloak>
  <div class="bg-primary" style="height:32px;">
    <p class="text-white text-right p-2 m-0" style="font-size: 10px">{{topBarText}}</p>
  </div>
  <nav id="nav" class="navbar sticky-top navbar-expand-lg page-menu <?php echo $menuExtraClass; ?>" style="z-index:1000; margin-top:32px;">

    <a class="navbar-brand" href="<?php echo $rootUrl . '/index.php' ?>">
      <img style="max-width: 265px;" src="<?php echo $rootDir . $logoUrl; ?>" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-bar bar1"></span>
      <span class="navbar-toggler-bar bar2"></span>
      <span class="navbar-toggler-bar bar3"></span>
    </button>

    <div class="collapse navbar-collapse justify-content-end " id="navbarNavDropdown">

      <ul class="navbar-nav" id="desktop-nav">
        <li class="nav-item <?php echo $aboutUsMenu; ?>">
          <a class="nav-link " href="<?php echo $rootUrl; ?>/quienes-somos.php">Quienes somos</a>
        </li>
        <li class="nav-item <?php echo $directoryMenu; ?>">
          <a class="nav-link" href="<?php echo $rootUrl; ?>/directorio.php">Directorio</a>
        </li>
        <li class="nav-item <?php echo $eventsMenu; ?>">
          <a class="nav-link" href="<?php echo $rootUrl; ?>/eventos.php">Eventos</a>
        </li>
        <!--     <li class="nav-item <?php echo $blogMenu; ?>">
          <a class="nav-link" href="<?php echo $rootUrl; ?>/blog.php">Blog</a>
        </li> -->
        <li class="nav-item">
          <a class="nav-link" href="https://cinepolis.com.gt/cartelera/guatemala-guatemala/cinepolis-portales" target="blank">Cinepolis</a>
        </li>
        <li class="nav-item <?php echo $benefitsMenu; ?>">
          <a class="nav-link" href="<?php echo $rootUrl; ?>/beneficios.php">Beneficios</a>
        </li>
        <li class="nav-item <?php echo $elCircoMenu; ?>">
          <a class="nav-link" href="<?php echo $rootUrl; ?>/el-circo.php">El Circo</a>
        </li>
        <!-- <li class="nav-item <?php echo $megaSaleMenu; ?>">
          <a class="nav-link" href="<?php echo $rootUrl; ?>/megasale/index.php">Megasale</a>
        </li> -->
        <li class="nav-item  d-none d-sm-none d-md-block <?php echo $servicesMenu; ?>">
          <a class="nav-link" id="services-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo $rootUrl; ?>/servicios.php">Servicios</a>
          <dl id="menu-services-popover" class="d-none">
            <dt>Parqueo</dt>
            <dd class="font-light">Zona de taxis</dd>
            <dd class="font-light">Parqueo de bicicletas</dd>
            <dt>Enfermería</dt>
            <dt>Sillas de ruedas</dt>
            <dt>Kiddies</dt>
            <dd class="font-light">Carritos</dd>
            <dd class="font-light">Amenidades</dd>
            <dt>Kiosco de información</dt>
            <dt>Wifi</dt>
          </dl>

        </li>
        <li class="nav-item d-block d-sm-block d-md-none  <?php echo $servicesMenu; ?>">
          <a class="nav-link" href="<?php echo $rootUrl; ?>/servicios.php">Servicios</a>


        </li>

        <!--  <li class="nav-item ">
          <a class="nav-link" href="<?php echo $rootUrl; ?>/buscador.php"><i class="now-ui-icons ui-1_zoom-bold"></i></a>
        </li> -->

      </ul>
    </div>


  </nav>
</div>

<!--  -->
<script type="module" src="<?php echo $rootDir; ?>/assets/js/scripts/menu.js"></script>