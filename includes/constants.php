<?php

$isDev = true;
$apiUrl = '';

$rootDir = '';
$rootUrl = '';



$pageName = 'Portales Mall';
$pageDescription = '';
$pageLogo = '';
$whatsappLink = 'https://wa.me/';
$fbMessengerLink = 'http://m.me/';
$faviconUrl = '/assets/img/brand/favicon.png';
$logoUrl = '/assets/img/brand/logo.svg';
$logoFooterUrl = '/assets/img/brand/footer-logo.png';



if($isDev) {
    //$apiUrl = 'http://localhost:1337/';
    $rootDir = '/malls-website-template';
    $rootUrl = 'http://localhost'.$rootDir;
    
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
} else {
    $rootUrl = 'https://portales.com.gt';
    $apiUrl = '';
    $rootDir = '/';
    $rootUrl = $rootUrl.$rootDir;
}



?>