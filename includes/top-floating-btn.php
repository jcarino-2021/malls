<div id="back-to-top-btn">
    <i class="fa fa-arrow-up"></i>
    <p class="font-light"> top </p>
</div>

<script>
    document.getElementById('back-to-top-btn').addEventListener('click', function() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
});
</script>