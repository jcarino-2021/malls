 <!-- Favicon -->
 <link rel="icon" href="<?php echo $rootDir . $faviconUrl; ?>" type="image/png" />
 
<!-- Bootstrap CSS -->
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

 <!-- NOW UI Kit CSS -->
 <link rel="stylesheet" href="<?php echo $rootDir; ?>/assets/css/now-ui-kit.css">

 <!-- Animate CSS -->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

 <!-- Icons -->
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
 <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCE9-8HYQJMRVZ8mZFVsyMWVK2kGRDmoKk&libraries=geometry" async></script> -->

 