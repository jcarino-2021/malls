<?php include('./includes/constants.php'); ?>
<!-- Constantes de la aplicación -->
<?php include('./includes/header.php'); ?>

<div id="app" v-cloak>
    <div class="container-fluid d-flex align-items-center justify-content-center" style="background-image: url('./assets/img/blog/header.png'); height: 550px; background-size: cover; background-position: center;">
        <h2 class="text-white font-light">Blog</h2>
    </div>
    <div class="container my-3">
    <div class="row my-5">
            <div class="col-md-12 text-center">
                <h2 class="mb-0">Trending now</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" v-if="blogData">
                <div class="owl-carousel owl-theme">
                    <div v-for="blogPost in blogData.posts.slice(0,3)">
                        <div class="col-md-12 p-0">
                            <!-- <img src="./assets/img/rooftop/info-section.png" alt="Reserva ya"> -->
                            <div style="background-size: cover" v-bind:style="{'background-image': 'url(' + blogPost.img + ')'}">
                                <div style="height: 500px"></div>
                            </div>
                            <div class="row" style="position:absolute; bottom: 5px; right: 15px; width: 100%; padding:15px 25px 0 25px;">
                                <div class="col-md-6 text-white">
                                    <h3 class="special-font-title mb-1">{{blogPost.title}}</h3>
                                    <p class="font-light" style="font-size: 0.8rem;">{{blogPost.caption}}</p>
                                </div>
                                <div class="col-md-6 text-right">
                                    <button class="btn btn-info btn-round" style="min-width: 150px;">Ver más</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container my-5">
        <div class="row">
            <div class="col-md-12">
                <h3 class="special-font text-center">Blog</h3>

            </div>
            <div class="col-md-4 text-center" v-for="blogPost in blogData.posts">
                <a style="text-decoration: none;" v-bind:href="'<?php echo $rootUrl ?>/articulos.php?art=' + blogPost.number">
                    <!-- <img :src="blogPost.img" alt=""> -->
                    <div style="background-size: cover" v-bind:style="{'background-image': 'url(' + blogPost.img + ')'}">   
                        <div style="height: 200px"></div>                    
                    </div>
                    <div class="mt-4 d-flex align-items-center justify-content-center" style="height: 55px; line-height: 18px !important;">
                        <p class="px-4" style="font-size: 1.3rem; width: 80%; color: #2c2c2c">
                            {{blogPost.title}}
                        </p>
                    </div>
                    <div stlye="height: 100px;">
                        <p class="text-muted font-light px-3" style="font-size: 0.7rem; height: 90px;">{{blogPost.caption}}</p>
                    </div>  
                </a>                              
            </div>
        </div>
    </div>
</div>


<?php include('./includes/footer-includes.php'); ?>
<!-- Load js used in this page -->
<script type="module" src="./assets/js/scripts/blog.js"></script>
<?php include('./includes/footer.php'); ?>