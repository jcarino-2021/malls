<?php include('./includes/constants.php'); ?>
<!-- Constantes de la aplicación -->
<?php include('./includes/header.php'); ?>

<div id="app" v-cloak>
    <div class="container-fluid d-flex align-items-center justify-content-center" v-bind:style="'background-image: url(' + postNumber.img + '); height: 550px; background-size: cover; background-position: center;'">
        <h2  class="text-white font-light">{{postNumber.title}}</h2>
    </div>
    <div class="container my-3">
    <!-- <div class="row my-5">
            <div class="col-md-12 text-center">
                <h2 class="mb-0">Trending now</h2>
            </div>
    </div> -->
        <div style="margin-top: 10%; border-bottom: 1px solid #C4C4C4"></div>
        <div style="margin-top: 30px" class="row">            
                <div style="margin-bottom: 20px" class="col-md-1 text-center">
                <a style="background-color: #C4C4C4; padding: 10px;" href="javascript:var dir=window.document.URL;var tit=window.document.title;var tit2=encodeURIComponent(tit);var dir2= encodeURIComponent(dir);window.location.href=('http://www.facebook.com/share.php?u='+dir2+'&amp;t='+tit2+'');"><img src="./assets/img/icons/facebook-footer.svg" border="0" width="32" height="32" alt="Compartir" /></a>
                </div>
                <div class="col-md-8">
                    <p class="text-muted font-light" style="color: #C4C4C4 !important; white-space: pre-line">{{postNumber.text_1}}</p>
                    <img style="width: 100%" v-bind:src="postNumber.img_1" alt="">
                    <p class="text-muted font-light" style="color: #C4C4C4 !important; margin-top: 30px; white-space: pre-line">{{postNumber.text_2}}</p>
                </div>
                <div style="margin-bottom: 20px" class="col-md-3 text-center">
                    <img style="background-color: #C4C4C4 !important; padding: 1px" src="./assets/img/brand/logo.svg" alt="">
                </div>            
        </div>

        <div class="row">            
                <div class="col-md-1">                
                </div>
                <div class="col-md-8">                    
                    <img style="width: 100%" v-bind:src="postNumber.img_2" alt="">                    
                </div>                           
        </div>
        <div class="row">            
                <div class="col-md-1">                
                </div>
                <div class="col-md-8">
                    <br/>
                <p class="text-muted font-light" style="color: #C4C4C4 !important; white-space: pre-line; text-align:justify">{{postNumber.text_3}}</p>                    
                    <img style="width: 100%" v-bind:src="postNumber.img_3" alt="">                    
                </div>                           
        </div>

        <div style="margin-top: 5%" class="row">
            <div class="col-md-12" v-if="blogData">
                <div class="owl-carousel owl-theme">
                    <div v-for="blogPost in blogData.posts.slice(0,31)" :key="index">
                        <div class="col-md-12 p-0">
                            <a style="text-decoration: none" v-bind:href="'<?php echo $rootUrl; ?>/articulos.php?art=' + blogPost.number">
                                <img v-bind:src="blogPost.img" alt="Reserva ya">                            
                                <h3 style="margin-top: 30px" class="special-font-title mb-1 text-center">{{blogPost.title}}</h3>
                                <p class="font-light text-center" style="font-size: 0.8rem; margin: 0px 50px">{{blogPost.caption}}</p>  
                            </a>                                                         
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="container my-5">
        <div class="row">
            <div class="col-md-12">
                <h3 class="special-font text-center">Blog</h3>

            </div>
            <div class="col-md-4 text-center" v-for="blogPost in blogData.posts">
                <img :src="blogPost.img" alt="">
                <div class="mt-4 d-flex align-items-center justify-content-center" style="height: 55px; line-height: 18px !important;">
                    <p class="px-4" style="font-size: 1.3rem; width: 80%;">
                        {{blogPost.title}}
                    </p>
                </div>
                <div stlye="height: 75px;">
                    <p class="text-muted font-light px-3" style="font-size: 0.7rem; height: 75px;">{{blogPost.caption}}</p>
                </div>


            </div>
        </div>
    </div>     -->
</div>


<?php include('./includes/footer-includes.php'); ?>
<!-- Load js used in this page -->
<script type="module" src="./assets/js/scripts/articles.js"></script>
<?php include('./includes/footer.php'); ?>