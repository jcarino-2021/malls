<?php include('./includes/constants.php'); ?>
<!-- Constantes de la aplicación -->
<?php include('./includes/header.php'); ?>

<div id="app" v-cloak>

    <div class="container-fluid d-flex align-items-center justify-content-center" style="background-image: url('./assets/img/directorio/header.png'); height: 550px; background-size: cover; background-position: center;">
        <h2 class="text-white special-font">Directorio</h2>
    </div>
    <div class="container-fluid my-5 py-5 bg-primary">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="text-white">Nuestro directorio</h2>
            </div>
        </div>
        <div class="row mx-auto" style="width: 85%;">
            <div class="col-md-12">
                <div class="input-group mb-3" id="directory-search-input" style="width:100%;text-align: center;margin-left: auto;margin-right: auto;">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="now-ui-icons ui-1_zoom-bold"></i></span>
                    </div>
                    <input type="text" v-on:keyup="filterStoresByTerm" class="form-control" placeholder="Ingresa palabra o tienda">
                </div>
            </div>
            <div class="col-md-12 mt-3">
                <ul class="directory-categories" v-if="storeCategories">
                    <li><span class="font-white font-light">Categorías relevantes</span> </li>
                    <li><span class="badge badge-primary" v-bind:class="{'badge-active': selectedCategory === 0}" v-on:click="filterStoresByCategoryId(0)">Todas</span> </li>
                    <li v-for="category in storeCategories.slice(0,numberOfCategoriesToShow)">
                        <span class="badge badge-primary" v-bind:class="{'badge-active': selectedCategory === category.category_id}" v-on:click="filterStoresByCategoryId(category.category_id)">{{category.name}}
                        </span>
                    </li>
                    <li><span class="font-white font-light" v-if="numberOfCategoriesToShow === defaultCategoriesToShow" v-on:click="numberOfCategoriesToShow = storeCategories.length">Ver más</span> </li>
                    <li><span class="font-white font-light" v-if="numberOfCategoriesToShow === storeCategories.length" v-on:click="numberOfCategoriesToShow = defaultCategoriesToShow">Ver menos</span> </li>

                </ul>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="text-center"><h2 class="special-font font-weight-bold">Tiendas</h2></div>
        <div class="row">
            <div class="col-md-4 text-center store-card my-3" v-for="store in filteredStores">
                <div class="image-container-store mx-auto">
                    <img :src="store.avatar" :alt="store.name">
                </div>
                <h4 class="font-bold mb-1 mt-1">{{store.name}}</h4>
                <p class="text-muted font-light store-location">{{store.malls.location}}</p>
                <div class="directory-store-socials">
                    <a v-if="store.web" :href="store.web" target="blank"><img src="./assets/img/icons/website-store.svg" alt="Web"></a>
                    <a v-if="store.phone_number" :href="'tel:' + store.phone_number"><img src="./assets/img/icons/phone-store.svg" alt="Phone"></a>
                    <a v-if="store.malls.whatsapp" :href="'https://wa.me/' + store.malls.whatsapp"><img src="./assets/img/icons/whatsapp-store.svg" alt="Whatsapp"></a>
                    <a v-if="store.facebook" :href="store.facebook" target="blank"><img src="./assets/img/icons/facebook-store.svg" alt="Facebook"></a>
                </div>
            </div>
            <div class="col-md-12 mt-5 text-center" v-if="!filteredStores">
                <img src="./assets//img/icons/loading.gif" alt="">
            </div>
            <div class="col-md-12 text-center" v-if="!filteredStores.length && !isLoading">
                <h2 class="special-font-title">No existen tiendas para esta búsqueda/categoría</h2>
            </div>
        </div>
    </div>

</div>


<?php include('./includes/footer-includes.php'); ?>
<!-- Load js used in this page -->
<script type="module" src="./assets/js/scripts/stores.js"></script>
<?php include('./includes/footer.php'); ?>