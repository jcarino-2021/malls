<?php include('./../includes/constants.php'); ?>
<!-- Constantes de la aplicación -->
<?php include('./../includes/header.php'); ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-FWFEDLEQHS"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-FWFEDLEQHS');
</script>
<div id="app" v-cloak>

    <div class="container-fluid d-flex p-0 align-items-center justify-content-center flex-column">
        <img id="megasale-image-header" class="w-100" src="./../assets/img/megasale/header.jpg" alt="">


    </div>
    <div class="container-fluid my-5 py-5 bg-primary">

        <div class="row mx-auto" style="width: 85%;">
            <div class="col-md-12">
                <div class="input-group mb-3" id="directory-search-input" style="width:100%;text-align: center;margin-left: auto;margin-right: auto;">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="now-ui-icons ui-1_zoom-bold"></i></span>
                    </div>
                    <input type="text" id="megasale-search-input" v-on:keyup="filterMegaSaleBenefitsByTerm" class="form-control" placeholder="Ingresa palabra o tienda">
                </div>
            </div>
            <!--   <div class="col-md-12 mt-3">
                <ul class="directory-categories" v-if="storeCategories.length > 0">
                    <li><span class="font-white font-light">Categorías relevantes</span> </li>
                    <li><span class="badge badge-primary" v-bind:class="{'badge-active': selectedCategory === 0}" v-on:click="filterStoresByCategoryId(0)">Todas</span> </li>
                    <li v-for="category in storeCategories.slice(0,numberOfCategoriesToShow)">
                        <span class="badge badge-primary" v-bind:class="{'badge-active': selectedCategory === category.category_id}" v-on:click="filterStoresByCategoryId(category.category_id)">{{category.name}}
                        </span>
                    </li>
                    <li><span class="font-white font-light" v-if="numberOfCategoriesToShow === defaultCategoriesToShow" v-on:click="numberOfCategoriesToShow = storeCategories.length">Ver más</span> </li>
                    <li><span class="font-white font-light" v-if="numberOfCategoriesToShow === storeCategories.length" v-on:click="numberOfCategoriesToShow = defaultCategoriesToShow">Ver menos</span> </li>

                </ul>
            </div> -->
        </div>
    </div>

    <div class="container">
        <div class="row mb-5">
            <div class="col-md-6 my-2 text-center">
                <select class="selectpicker spCategories" v-model="selectedCategory" data-style="select-with-transition btn-outline-primary" data-live-search="true" title="Categorías" data-size="7" @change="filterBenefitsBySingleCategoryId">
                    <option disabled>Categorías</option>
                    <option v-for="category in storeCategories" :value="category.category_id">{{category.name}}</option>

                </select>
            </div>
            <div class="col-md-6 my-2 text-center">
                <select class="selectpicker spStores" v-model="selectedStore" data-style="select-with-transition btn-outline-primary" data-live-search="true" title="Tiendas" data-size="7" @change="filterBenefitsBySingleStoreId">
                    <option disabled>Tiendas</option>
                    <option v-for="store in stores" :value="store.store_id">{{store.name}}</option>
                </select>
            </div>
            <div class="col-md-12 text-center">
                <button class="btn btn-primary" type="button" v-on:click="clearFilters()">Limpiar filtros</button>
            </div>

        </div>

        <div class="row">
            
            <div class=" col-md-4 col-lg-3 text-center store-card my-3" v-for="megaSaleBenefit in specialBenefitsMegaSaleFiltered">
                <div class="image-container-store mx-auto">
                    <img :src="megaSaleBenefit.store.avatar" loading="lazy" :alt="megaSaleBenefit.store.store_name">
                </div>
                <h4 class="font-bold mb-1 mt-1">{{megaSaleBenefit.name}}</h4>
                <p class="text-muted font-light store-location">{{megaSaleBenefit.description}}</p>
            </div>
            
            <div class="col-md-12 mt-5 text-center" v-if="isLoading">
                <img src="./../assets/img/icons/loading.gif" alt="">
            </div>
            <div class="col-md-12 text-center" v-if="!specialBenefitsMegaSaleFiltered.length && !isLoading">
                <h2 class="special-font-title">No existen tiendas para esta búsqueda/categoría</h2>
            </div>
        </div>
    </div>

</div>


<?php include('./../includes/footer-includes.php'); ?>
<!-- Load js used in this page -->
<script type="module" src="./../assets/js/scripts/megasale.js"></script>
<?php include('./../includes/footer.php'); ?>