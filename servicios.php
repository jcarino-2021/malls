<?php include('./includes/constants.php'); ?>
<!-- Constantes de la aplicación -->
<?php include('./includes/header.php'); ?>

<div id="app" v-cloak>
    <!-- Slider -->
    <div class="container-fluid d-flex align-items-center justify-content-center" style="background-image: url('./assets/img/servicios/header.png'); height: 550px; background-size: cover; background-position: center;">
        <h2 class="text-white font-light">Servicios</h2>
    </div>
    <div class="container my-5">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Parqueo</h2>
                <p class="text-muted font-light mb-0">¡Parquea sin complicarte! Identifica los estacionamientos disponibles que te indican las señales y listo. </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center my-2">
                <h4 class="font-light mb-0">Tarifas</h4>
            </div>
            <div class="col-md-6 col-lg-4" v-for="fare in servicesData.fares">

                <div class="text-center directory-home-item mx-auto">
                    <img :src="fare.img" style="margin-top: 0" alt="">
                    <p class="font-light" style="position:initial !important;">{{fare.title}}</p>
                </div>
                <p class="text-muted font-light text-center" style="padding:0 30px; font-size: 0.9rem;">{{fare.detail}}</p>

            </div>

        </div>
        

    </div>
    
    <div class="container-fluid my-5 left-image-right-content">
        <div class="row my-4">
            <div class="col-md-6 px-0">
                <img src="./assets/img/servicios/clinica.png" alt="">
            </div>
            <div class="col-md-6 d-flex flex-column align-items-center justify-content-center mt-sm-5">
                <div class="content-side">
                    <h3 class="font-light">Servicios</h3>
                    <h2 class="font-light">Clínica</h2>
                    <p class="text-muted font-light">Con nosotros puedes estar seguro, contamos con profesionales que te cuidan en caso de necesitar cualquier servicio de enfermería. Encuentra nuestra clínica en el Nivel 1A.</p>
                </div>

            </div>
        </div>
    </div>
    <div class="container-fluid my-10 right-image-left-content ">
        <div class="row my-4">
            <div class="col-md-6 d-flex flex-column align-items-center justify-content-center  order-2 order-md-1 order-sm-2 mt-sm-5">
                <div class="content-side">
                    <h3 class="font-light">Servicios</h3>
                    <h2 class="font-light">Silla de ruedas</h2>
                    <p class="text-muted font-light">Contamos con sillas de ruedas para facilitar tu recorrido en Portales Mall. Encuéntralas en el kisoco de información dentro del Centro Comercial en el Primer Nivel, Plaza central (a la par de San Martín)</p>
                </div>
            </div>
            <div class="col-md-6 px-0 order-1 order-sm-1  order-md-2">
                <img src="./assets/img/servicios/silla-ruedas.png" alt="">
            </div>
        </div>
    </div>
    <div class="container-fluid my-5 left-image-right-content">
        <div class="row my-4">
            <div class="col-md-6 px-0">
                <img src="./assets/img/servicios/amenidades.png" alt="">
            </div>
            <div class="col-md-6 d-flex flex-column align-items-center justify-content-center mt-sm-5">
                <div class="content-side">
                    <h3 class="font-light">Servicios</h3>
                    <h2 class="font-light">Amenidades y servicios</h2>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="mb-1 text-muted font-light">Kiddies</p>
                            <p class="mb-1 text-muted font-light">¡Deja que tus hijos rueden por todo el mall! Nuestros carritos están disponibles para que pases una divertida tarde en familia.</p>
                            <p class="font-light text-muted">
                                <img src="./assets//img/icons/money.svg" style="width: 25px;" alt="">
                                Tarifa: Q15 hora o fracción, Q15 individual, Q20 doble. 
                            </p>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-md-6">
                            <p class="mb-1 text-muted font-light">Amenidades</p>
                            <p class="mb-1 text-muted font-light">Carrousel, Tren, Soft Play.</p>
                            <p class="mb-2 text-muted font-light" style="font-size:1rem;">
                            "Por el momento nuestras amenidades no están abiertas hasta que las regulaciones por COVID nos permitan abrir."
                            </p>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <div class="container-fluid my-10 right-image-left-content ">
        <div class="row my-4">
            <div class="col-md-6 d-flex flex-column align-items-center justify-content-center order-2 order-md-1 order-sm-2 mt-sm-5">
                <div class="content-side">
                    <h3 class="font-light">Servicios</h3>
                    <h2 class="font-light">Kiosco de información</h2>
                    <p class="text-muted font-light">Contamos con Kiosco de Información para poder resolver cualquer duda que tengas relacionada a los horarios, precios, o de cómo aprovechar al máximo tu experiencia en Portales. </p>
                    <div class="my-5">
                        <img src="./assets/img/icons/clock.svg" style="width:25px; position: absolute;" alt="">
                        <div class="ml-5">
                            <p class="text-muted font-light">
                                Lunes a jueves de 10:00 am a 8:00 pm
                            </p>
                            <p class="text-muted font-light">
                                Viernes y sábado de 10:00 am a 9:00 pm
                            </p>
                            
                            <p class="text-muted font-light">
                                Domingo y festivos de 10:00 am a 8:00 pm
                            </p>

                        </div>

                    </div>
                    <div class="mt-5">
                        <img src="./assets/img/icons/pin.svg" style="width:25px; position: absolute;" alt="">
                        <div class="ml-5">
                            <p class="text-muted font-light">
                                Primer nivel, Plaza Principal (al lado de San Martín)
                            </p>

                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-6 px-0 order-1 order-sm-1  order-md-2">
                <img src="./assets/img/servicios/kioskos.png" alt="">
            </div>
        </div>
    </div>

</div>


<?php include('./includes/footer-includes.php'); ?>
<!-- Load js used in this page -->
<script type="module" src="./assets/js/scripts/services.js"></script>
<?php include('./includes/footer.php'); ?>