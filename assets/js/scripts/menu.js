import { configurations } from '../configurations.js';
import { jsonData } from '../data/data.js';

const app = new Vue({
  el: '#app-menu',
  data() {
    return {
      topBarText: ''

    }
  },
  mounted() {
    this.topBarText = jsonData.topbar.schedule;
  }
});

window.addEventListener('scroll', () => {
  if (window.scrollY > 70) {
    const menuEl = document.getElementsByClassName('navbar')[0];
    menuEl.classList.add('bg-transition-primary');
    menuEl.style.marginTop = '0px';
  } else if (window.scrollY < 70) {
    const menuEl = document.getElementsByClassName('navbar')[0];
    menuEl.classList.remove('bg-transition-primary');
    menuEl.style.marginTop = '32px';
  }
});

document.getElementById('services-dropdown').addEventListener('mouseover', () => {
  showServicesPopover();
  animateServicesPopover('slideDown');
});
document.getElementById('services-dropdown').addEventListener('mouseout', () => {
  const hideTimeout = setTimeout(() => {
    hideServicesPopover();
    animateServicesPopover('slideUp');
  }, 1200);
  const popoverMenuEl = document.getElementById('menu-services-popover');
  popoverMenuEl.addEventListener('mouseover', () => {
    clearTimeout(hideTimeout);
    showServicesPopover();

  });
  popoverMenuEl.addEventListener('mouseout', () => {
    hideServicesPopover();
    animateServicesPopover('slideUp');
  });
  popoverMenuEl.addEventListener('click', () => {
    const pathName = window.location.pathname;

    if(pathName.toLowerCase().includes('megasale')) {
      window.location.href= "../servicios.php";
    } else {
      window.location.href= "servicios.php";
    }
  });
});

function animateServicesPopover(animation) {
  const popoverMenuEl = document.getElementById('menu-services-popover');
  animateCSS(popoverMenuEl, animation, '0.5s');
}

function hideServicesPopover() {
  const popoverMenuEl = document.getElementById('menu-services-popover');
  popoverMenuEl.classList.add('d-none');
  
}

function showServicesPopover() {
  const popoverMenuEl = document.getElementById('menu-services-popover');
  popoverMenuEl.classList.remove('d-none');
}