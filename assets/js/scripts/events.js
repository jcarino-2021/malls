import { configurations } from '../configurations.js';
import { slider } from '../components/slider.component.js';



import { jsonData } from '../data/data.js';

const app = new Vue({
  el: '#app',
  data() {
    return {
      upcomingEvents: '',
      
      
      
    }
  },
  async mounted() {
    this.upcomingEvents = await fetch(`${configurations.apiUrl}mall_events?filter[mall_id]=${configurations.mall_id}`);
    this.upcomingEvents = await this.upcomingEvents.json();
    this.upcomingEvents = this.upcomingEvents.data;
    console.log(this.upcomingEvents);
  }
});