import { configurations } from '../configurations.js';
import { jsonData } from '../data/data.js';

const app = new Vue({
  el: '#app',
  data() {
    return {
      storeCategories: '',
      stores: '',
      filteredStores: '',
      numberOfCategoriesToShow: 5,
      defaultCategoriesToShow: 5,
      selectedCategory: 0,
      searchDebounce: '',
      isLoading: true
    }
  },
  methods: {
    getCategories: async function () {
      this.storeCategories = await fetch(`${configurations.apiUrl}categories?filter[mall_id]=${configurations.mall_id}&per_page=500`);
      this.storeCategories = await this.storeCategories.json();
      this.storeCategories = this.storeCategories.data;
      localStorage.setItem(`Spec_mallCategories${configurations.mall_id}`, JSON.stringify(this.storeCategories));
    },
    getStores: function () {
      return new Promise(async resolve => {
        this.stores = await fetch(`${configurations.apiUrl}stores?filter[mall_id]=${configurations.mall_id}&per_page=500`);
        this.stores = await this.stores.json();
        this.stores = this.stores.data;
        this.stores.forEach(store => {
          store.malls = store.malls.find(mall => mall.mall_id === configurations.mall_id);
        });

        localStorage.setItem(`Spec_mallStores${configurations.mall_id}`, JSON.stringify(this.stores));
        localStorage.setItem(`Spec_mallStoresRefresh${configurations.mall_id}`, Date.now())
        resolve(true);
      })
    },
    filterStoresByTerm: function (event, searchTerm) {
      const term = event?event.srcElement.value: searchTerm;
      clearTimeout(this.searchDebounce);

      this.searchDebounce = setTimeout(() => {
        this.filterStoresByCategoryId(this.selectedCategory);
        this.filteredStores = this.filteredStores.filter(store => store.name.toLowerCase().includes(term.toLowerCase()));
      }, 300);

    },
    filterStoresByCategoryId: function (id) {
      console.log(id);
      this.selectedCategory = id;
      this.filteredStores = this.stores.slice();
      if (id > 0) {
        this.filteredStores = this.filteredStores.filter(store => store.categories.find(category => category.category_id === id));
      }

    }
  },
  async mounted() {

    /* this.searchDebounce = setTimeout(()=>{},100); */
    const storedCategories = JSON.parse(localStorage.getItem(`Spec_mallCategories${configurations.mall_id}`));
    const storedStores = JSON.parse(localStorage.getItem(`Spec_mallStores${configurations.mall_id}`));
    if (!storedCategories || !storedStores) {
      this.getCategories();
      await this.getStores();
    } else {
      const refreshTimestamp = parseInt(localStorage.getItem(`Spec_mallStoresRefresh${configurations.mall_id}`), 10);
      const now = Date.now();
      //Si el tiempo es mayor a x segundos se refrescan las tiendas y categorias
      if ((now - refreshTimestamp) / 1000 > 600) {
        this.getCategories();
        await this.getStores();
      } else {
        this.storeCategories = storedCategories;
        this.stores = storedStores;
      }
    }
    this.filteredStores = this.stores.slice();
    const query = new URLSearchParams(window.location.search);
    if(query.has('category')) {
      this.selectedCategory = parseInt(query.get('category'));
    }
    if (query.has('term')) {
      console.log(query.get('term'));
      this.filterStoresByTerm(false, query.get('term'));
    }

    this.isLoading = false;
  }
});