import { configurations } from '../configurations.js';

import { jsonData } from '../data/data.js';

const app = new Vue({
  el: '#app',
  data() {
    return {
      coupons: '',
      specialBenefits: '',
      selectedCoupon: ''
    }
  },
  methods: {
    selectCouponAndOpenModal: function(coupon) {
      this.selectedCoupon = coupon;
      console.log(coupon);
      $('#couponModal').modal('show');
    }
  },
  async mounted() {
    this.coupons = await fetch(`${configurations.apiUrl}mall_coupons?filter[mall_id]=${configurations.mall_id}`);
    this.coupons = await this.coupons.json();
    this.coupons = this.coupons.data;

    //!TODO Cambiar a url de producción
    this.specialBenefits = await fetch(`${configurations.apiUrl}special_benefits?filter[mall_id]=${configurations.mall_id}`);
    this.specialBenefits = await this.specialBenefits.json();
    this.specialBenefits = this.specialBenefits.data;
    
    console.log("asdf", this.specialBenefits.data)

  }
});