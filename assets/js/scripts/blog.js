import { configurations } from '../configurations.js';
import { jsonData } from '../data/data.js';

const app = new Vue({
  el: '#app',
  data() {
    return {
      blogData: ''
    }
  },
  methods: {
    initializeCarousel: function () {
      setTimeout(() => {
        
        $(".owl-carousel").owlCarousel({
          nav: true,
          dots: false,
          loop: true,
          autoplay: true,
          autoplayTimeout: 5000,
          responsive: {
            0: {
              items: 1
            },
            600: {
              items: 1
            },
            1000: {
              items: 1
            }
          }
        });
      }, 50)
    }
  },
  mounted() {
    this.blogData = jsonData.blog;
    this.initializeCarousel();
  }
});