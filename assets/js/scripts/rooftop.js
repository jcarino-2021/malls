import { configurations } from '../configurations.js';
import { jsonData } from '../data/data.js';

const app = new Vue({
  el: '#app',
  data() {
    return {
      rooftopData: '',
      selectedCategory: '',
      selectedCategoryIndex: 3
    }
  },
  methods: {
    selectCategory: function(index, category) {
      $('.owl-carousel').trigger('destroy.owl.carousel');  
      this.selectedCategory = category;
      this.selectedCategoryIndex = index;
      //this.refreshCarousel();
      this.initializeCarousel();
    },
    initializeCarousel: function() {
      setTimeout(() => {
        $(".owl-carousel").owlCarousel({
          margin: 15,
          nav: true,
          dots: false,
          responsive: {
            0: {
              items: 1
            },
            600: {
              items: 3
            },
            1000: {
              items:4
            }
          }
        });
      }, 50)
    }
  },
  async mounted() {
    this.rooftopData = jsonData.rooftop;
    this.selectedCategory = this.rooftopData.categories[this.selectedCategoryIndex];
    this.initializeCarousel();
    
    
  }
});