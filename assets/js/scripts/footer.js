import { configurations } from '../configurations.js';
import { formFeedbackComponent } from '../components/form-feedback.component.js';
import { jsonData } from '../data/data.js';

const { required, email } = window.validators;

const app = new Vue({
  el: '#app-footer',
  data() {
    return {
      footerData: '',
      formSubmittedValid: false,
      newsletter_email: '',
      newsLetterRequestResponseText: ''
    }
  },
  validations() {
    return {
      newsletter_email: {
        required,
        email
      }
    }

  },
  methods: {
    submitNewsletter: async function (e) {
      console.log('Newsletter ');
      this.$v.$touch();
      if (this.$v.$invalid) {
        this.submitStatus = 'ERROR'
      } else {
        this.submitStatus = 'PENDING'

        const currentElementHtml = e.submitter.innerHTML;
        loadingIcon(e.submitter);


        const page = window.location.origin;
        const uri = window.location.pathname;

        let newSuscription = await fetch('https://api-lcs-dev.lumationsuite.com/api/v1/newsletter/public', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            email: this.newsletter_email,
            page: page,
            uri: uri
          })
        });

        newSuscription = await newSuscription.json();

        if (newSuscription.success) {
          this.formSubmittedValid = 200;
          this.newsLetterRequestResponseText = '¡Has sido registrado exitosamente!'
        } else {
          this.formSubmittedValid = 400;
          this.newsLetterRequestResponseText = '¡Parece que ha ocurrido un error. Intenta nuevamente!'
        }

        stopLoadingIcon(e.submitter, currentElementHtml);

      }
    }
  },
  mounted() {
    this.footerData = jsonData.footer;
  }
});