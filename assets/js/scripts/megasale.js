import { configurations } from '../configurations.js';
import { jsonData } from '../data/data.js';

const app = new Vue({
  el: '#app',
  data() {
    return {
      storeCategories: '',
      stores: '',
      filteredStores: [],
      numberOfCategoriesToShow: 5,
      defaultCategoriesToShow: 5,
      selectedCategory: 0,
      selectedStore: 0,
      searchDebounce: '',
      isLoading: true,
      specialBenefitsMegaSale: [],
      specialBenefitsMegaSaleFiltered: []
    }
  },
  methods: {
    getCategories: async function () {

      let categories = await fetch(`${configurations.apiUrl}categories?filter[mall_id]=${configurations.mall_id}&per_page=500`);
      categories = await categories.json();
      this.storeCategories = categories.data;
      localStorage.setItem(`Spec_mallCategories${configurations.mall_id}`, JSON.stringify(this.storeCategories));
      $('.spCategories').selectpicker('refresh');
      setTimeout(() => {
        $('.spCategories').selectpicker('refresh');
      }, 200);
    },
    getStores: function () {
      return new Promise(async resolve => {
        let stores = await fetch(`${configurations.apiUrl}stores?filter[mall_id]=${configurations.mall_id}&per_page=500`);

        stores = await stores.json();
        this.stores = stores.data;
        this.stores.forEach(store => {
          store.malls = store.malls.find(mall => mall.mall_id === configurations.mall_id);
        });
        for (let i = 0; i < this.specialBenefitsMegaSale.length; i++) {
          const benefit = this.specialBenefitsMegaSale[i];
          benefit.storeInfo = this.stores.find(store => store.name.toLowerCase() === benefit.store.store_name.toLowerCase());;

        }
        localStorage.setItem(`Spec_mallStoresMegaSale${configurations.mall_id}`, JSON.stringify(this.specialBenefitsMegaSale));
        localStorage.setItem(`Spec_mallStores${configurations.mall_id}`, JSON.stringify(this.stores));
        localStorage.setItem(`Spec_mallStoresRefresh${configurations.mall_id}`, Date.now());
        setTimeout(() => {
          $('.spStores').selectpicker('refresh');
        }, 200);
        
        resolve(true);
      })
    },
    getMegaSaleBenefits: async function () {
      return new Promise(async resolve => {
        this.specialBenefitsMegaSale = await fetch(`https://api.myfrictionless.com/api/public/special_benefits?filter[mall_id]=${configurations.mall_id}&filter[special_event_id]=1&per_page=500`);
        this.specialBenefitsMegaSale = await this.specialBenefitsMegaSale.json();
        this.specialBenefitsMegaSale = this.specialBenefitsMegaSale.data;
        resolve(true);
      })

    },
    filterMegaSaleBenefitsByTerm: function (event, searchTerm) {
      const term = event ? event.srcElement.value : searchTerm;
      clearTimeout(this.searchDebounce);

      this.searchDebounce = setTimeout(() => {
        this.filterBenefitsBySingleCategoryId(this.selectedCategory);
        this.specialBenefitsMegaSaleFiltered = this.specialBenefitsMegaSaleFiltered.filter(benefit => benefit.store.store_name.toLowerCase().includes(term.toLowerCase()) || benefit.description.toLowerCase().includes(term.toLowerCase()));
        console.log(this.specialBenefitsMegaSaleFiltered);
      }, 300);
    },
    clearFilters: function () {
      console.log('Test clear');
      this.selectedCategory = 0;
      this.selectedStore = 0;
      $('.spCategories').selectpicker('val', 0);
      $('.spStores').selectpicker('val', 0);
      document.getElementById('megasale-search-input').value = "";


    },
    filterBenefitsBySingleCategoryId: function (event, categoryId) {
      const id = event ? parseInt(event.srcElement.value) : parseInt(categoryId);
      console.log(id);
      this.selectedCategory = id;

      this.specialBenefitsMegaSaleFiltered = this.specialBenefitsMegaSale.slice();
      if (id > 0) {
        this.specialBenefitsMegaSaleFiltered = this.specialBenefitsMegaSaleFiltered.filter(benefit => benefit.storeInfo.categories.find(category => category.category_id === this.selectedCategory));
      }

    },
    filterBenefitsBySingleStoreId: function (event, storeId) {
      const id = event ? parseInt(event.srcElement.value) : parseInt(storeId);
      console.log(id);
      this.selectedStore = id;

      this.specialBenefitsMegaSaleFiltered = this.specialBenefitsMegaSale.slice();
      if (id > 0) {
        this.specialBenefitsMegaSaleFiltered = this.specialBenefitsMegaSaleFiltered.filter(benefit => benefit.storeInfo.store_id === this.selectedStore);
      }

    }


  },
  async mounted() {
    document.getElementById('nav').classList.add('megasale-menu');
    console.log(this.storeCategories);
    const storedCategories = JSON.parse(localStorage.getItem(`Spec_mallCategories${configurations.mall_id}`));
    const storedStores = JSON.parse(localStorage.getItem(`Spec_mallStores${configurations.mall_id}`));
    const storedMegaSaleBenefits = JSON.parse(localStorage.getItem(`Spec_mallStoresMegaSale${configurations.mall_id}`));
    if (!storedCategories || !storedStores || !storedMegaSaleBenefits) {
      await this.getMegaSaleBenefits();
      this.getCategories();
      this.getStores();
      
    } else {
      const refreshTimestamp = parseInt(localStorage.getItem(`Spec_mallStoresRefresh${configurations.mall_id}`), 10);
      const now = Date.now();
      //Si el tiempo es mayor a x segundos se refrescan las tiendas y categorias
      if ((now - refreshTimestamp) / 1000 > 500) {
        await this.getMegaSaleBenefits();
        this.getCategories();
        this.getStores();
        
      } else {
        this.storeCategories = storedCategories;
        this.stores = storedStores;
        this.specialBenefitsMegaSale = storedMegaSaleBenefits
      }
    }
    this.filteredStores = this.stores.slice();
    this.specialBenefitsMegaSaleFiltered = this.specialBenefitsMegaSale.slice();
    this.isLoading = false;
    console.log(this.specialBenefitsMegaSale.filter(benefit => benefit.storeInfo));

    setTimeout(() => {
      $('.spCategories').selectpicker('refresh');
      $('.spStores').selectpicker('refresh');
    }, 200)
    
  }
});