import { configurations } from '../configurations.js';
import { jsonData } from '../data/data.js';

const app = new Vue({
  el: '#app',
  data() {
    return {
      servicesData: ''
      
      
    }
  },
  mounted() {
    this.servicesData = jsonData.services;
  }
});