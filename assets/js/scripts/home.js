import { configurations } from '../configurations.js';
import { slider } from '../components/slider.component.js';



import { jsonData } from '../data/data.js';

const app = new Vue({
  el: '#app',
  data() {
    return {
      homeData: '',
      footerData: '',
      benefit: '',
      upcomingEvent: ''
      
    }
  },
  async mounted() {
    this.homeData = jsonData.home;
    this.footerData = jsonData.footer;
    this.upcomingEvent = await fetch(`${configurations.apiUrl}mall_events?filter[mall_id]=${configurations.mall_id}&per_page=1`);
    this.upcomingEvent = await this.upcomingEvent.json();
    this.upcomingEvent = this.upcomingEvent.data[0]?this.upcomingEvent.data[0]:[];

    this.benefit = await fetch(`${configurations.apiUrl}special_benefits?filter[mall_id]=${configurations.mall_id}&per_page=1`);
    this.benefit = await this.benefit.json();
    this.benefit = this.benefit.data[0];
  }
});