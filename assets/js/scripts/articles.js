import { configurations } from '../configurations.js';
import { jsonData } from '../data/data.js';

const app = new Vue({
  el: '#app',
  data() {
    return {
      blogData: '',
      postNumber: ''
    }
  },
  methods: {
    initializeCarousel: function () {
      setTimeout(() => {
        
        $(".owl-carousel").owlCarousel({
          nav: true,
          margin: 20,
          dots: false,
          loop: true,
          autoplay: true,
          autoplayTimeout: 5000,          
          responsive: {
            0: {
              items: 1,
              nav: true
            },
            768: {
              items: 2,
              nav: true
            },
            1024: {
              items: 3,
              nav: true,
            }
          }
        });
      }, 50)
    },

    number_post : function (){
        const valores = window.location.search;

        const Param = new URLSearchParams(valores);

        var value_url = Param.get('art');

        return value_url;
    }
  },
  mounted() {
    this.blogData = jsonData.blog;
    this.initializeCarousel();    

    this.postNumber = '';

    this.blogData.posts.forEach(post => {
        if(post.number == this.number_post()){
            this.postNumber = post;
        }
    });
    
  }
});