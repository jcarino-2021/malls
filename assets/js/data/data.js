const jsonData = {
    topbar: {
        schedule: 'Domingo a Jueves: 10:00am a 8:00pm | Viernes y Sábado: 10:00am a 9:00pm | Horario especial Adulto Mayor: 10:00am a 11:00am | Servicio de supermercado a partir de las 7:00am'
    },
    footer: {
        map: {
            img: './assets/img/footer/location-map.png',
            action_url: 'https://google.com'
        },
        schedule: [{
            title: 'Portales',
            opening_hours: [
                {
                    days: 'Dom a Jue',
                    schedule: '10am - 8pm'
                },
                {
                    days: 'Vie y Sáb',
                    schedule: '10am - 9pm'
                }
            ]
        }],
        social_networks: [{
            logo: './assets/img/icons/facebook-footer.svg',
            url: 'https://es-la.facebook.com/portalesgt/'
        },
        {
            logo: './assets/img/icons/instagram-footer.svg',
            url: 'https://www.instagram.com/portalesgt/?hl=es-la'
        },
        {
            logo: './assets/img/icons/waze-footer.svg',
            url: 'https://www.waze.com/es-419/live-map/directions/centro-comercial-portales-c.c.-portales-zona-17,-guatemala?to=place.w.176619666.1766327737.584661'
        },
        {
            logo: './assets/img/icons/google-maps-footer.svg',
            url: 'https://www.google.com/maps/place/Centro+Comercial+Portales+Guatemala/@14.647186,-90.4824339,18z/data=!4m12!1m6!3m5!1s0x8589a2858b795577:0x148c20c396c75cd8!2sCentro+Comercial+Portales+Guatemala!8m2!3d14.6470433!4d-90.4817633!3m4!1s0x8589a2858b795577:0x148c20c396c75cd8!8m2!3d14.6470433!4d-90.4817633'
        },
        /* {
            logo: './assets/img/icons/tiktok-footer.svg',
            url: 'https://google.com'
        }, */
   /*      {
            logo: './assets/img/icons/youtube-footer.svg',
            url: 'https://google.com'
        } */
        ]
    },
    home: {
        slides: [{
            "title": "",
            "subtitle": "",
            "call_to_action_text": "",
            "call_to_action_url": "",
            "is_fullscreen": false,
            "is_video": true,
            "video_overlay_color": "rgba(0, 0, 0, 0.1)",
            "image": {
                url: './assets/img/home/header.png'
            },
            "video":
            {
                "id": 50,
                "name": "productos.mp4",
                "alternativeText": "",
                "caption": "",
                "width": null,
                "height": null,
                "formats": null,
                "hash": "productos_9cd152e81d",
                "ext": ".mp4",
                "mime": "video/mp4",
                "size": 1260.94,
                "url": "https://portales.com.gt/assets/img/home/PM-header.mp4",
                "previewUrl": null,
                "provider": "firebase",
                "provider_metadata": null,
                "created_at": "2021-09-14T18:58:10.859Z",
                "updated_at": "2021-09-14T18:58:10.876Z"
            }

        }],
        blog_posts: [
            {
                img: './assets/img/home/blog-1.png',
                title: 'Mantener un buen autoestima',
                caption: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.'                
            },
            {
                img: './assets/img/home/blog-2.png',
                title: 'Espacios para compartir',
                caption: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.'
            },
            {
                img: './assets/img/home/blog-3.png',
                title: '¿Cómo vivir momentos únicos?',
                caption: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.'
            }
        ]
    },
    blog: {
        posts: [
            {
                img: './assets/img/articulos/art_01/img-noticias2.png',
                title: 'Los 10 hombres mejor vestidos… desde casa',
                caption: 'Te compartimos este artículo de GQ México para que te animes a vestir igual a estos famosos en la cuarentena: Cuando eres uno de los hombres mejor vestidos, nada te detiene.',
                text_1: 'Te compartimos este artículo de GQ México para que te animes a vestir igual a estos famosos en la cuarentena: Cuando eres uno de los hombres mejor vestidos, nada te detiene. Especialmente en esta época de estar en casa 24/7… ¿o 24/40? Hoy más que nunca estamos atentos a redes sociales y muchas de estas figuras públicas que os da un par de consejos en cada evento, alfombra roja o en una escapada al súper, no nos han defraudado. Una de las máximas recomendaciones durante este home office que hemos seguido día a día, es seguir nuestra rutina diaria de “grooming”, aseo personal y claro, sacar una buena camisa, joggers o una t-shirt del clóset para sentirnos al 200% a la hora de trabajar. Ahora la lista de personas que usamos las plataformas digitales para reuniones, entrevistas, sesiones con nuestros socios o incluso para hablar con la familia, tenemos que vernos bien. Estos son nuestros favoritos que, además han adoptado el grandioso experimento de jugar con su pelo y barba… sin dejar de ser los hombres mejor vestidos en casa. Así que deja ya la misma pijama de todos los días y ponte a explorar como ellos:',
                img_1: '',
                text_2: `1. Jared Leto
                Antes de iniciar la cuarentena, Jared leto estaba en un retiro espiritual y al salir… el mundo era completamente otro para él. Mr. 30 Seconds To Mars no falla ni en casa, pero pues ¿cómo?, si es uno de los hombres que mejor expresan su estilo. Joggers satinados y una graphic tee.
                
                2. David Beckham
                Para nosotros, nada es más “de casa” que una plaid shirt… a gusto, y siempre están en tendencia. Ahora puedes descansar de tus accesorios a diario, pero una buena boina o un dad hat te va a sacar de un apuro si tu pelo está descontrolado.
                
                3. Johnny Knoxville
                Presumiendo sus canas a los 49 años con orgullo. El señor líder de Jackass llegó al punto de muchos: corte de pelo al ras. Casi casi, como lo dice él, al mismo tamaño que su barba. Muy válido y muy buen momento para probar. Ah, y uno más comprobando las graphic tees como esencial de la cuarentena.
                
                4. Post Malone
                Bueno, él sí está siguiendo todas las reglas: dat hat, camisa con denim y un buen par de botas para salir… al jardín o al rooftop. Casual, trendy y cómodo para home office. ¡Aplausos a Post Malone!
                
                5. Ricky Martin
                Muchos de nosotros lo estamos aplicando para hacer ejercicio o en la sala, en el roof o en el jardín: “el estilo atheisure”. Ricky además jugó con el layering, muchos puntos por este look al papá de dos que ni la cuarentena lo detiene. Nota: a muchos (incluidos nosotros), vestirse bien les eleva los niveles de felicidad.
                
                6. Ryan Reynolds
                El favorito de muchos, él ha hecho de todo en casa: camisa lisa, t-shirt, accesorios… aunque, ¿afectará que su suegra esté viviendo con él durante la pandemia? Negativo, apostamos más por el lado de tener a Blake Lively en casa.
                
                7. Armie Hammer
                Ok, él tiene ese go para hacer lo que quiera con su look, y todos lo hemos hecho al momento que queremos cortarnos el pelo o la barba, seamos honestos. No todo es seriedad al estar en casa (si te lo permiten tus actividades), aunque a veces es bueno no cortarte el pelo en casa.
                
                8. J Balvin
                Volviendo al punto pasado comodidad, ante todo. El máster del reggaetón nos cuenta que se vale andar en bata un buen rato del día. Si tienes una, está perfecta para que después de tu jornada de home office, te lances a ver una las mejores series de terror del 2020.
                
                9. John Krasinski
                Si queremos todas las opciones anteriores, pero hay que admitir que ésta es una gran manera para poder sobrevivir las interminables juntas de todos los días… la comodidad también te dará “happy endorphines”, pero nunca dejes de estar presentable en la parte superior.
                
                10. Sting
                Como el cantante, un buen par de jeans salva todo: una cita con un cliente, una amorosa o una en estas aplicaciones digitales. Además, que te vas a ver chic en t-shirt, calcetines y mezclilla que eso nunca está peleado con estar bajo techo.`,
                img_2: '',
                number: 1
            },
            {
                img: './assets/img/articulos/art_02/img-noticias1.png',
                title: '¿La cuarentena nos hará mejores personas?',
                caption: 'De todas las experiencias raras e intensas de los últimos dos meses, en medio de la ansiedad, el horror, el aislamiento y la fiebre de la cabina, un aspecto ha sido especialmente extraño.',
                text_1:  `De todas las experiencias raras e intensas de los últimos dos meses, en medio de la ansiedad, el horror, el aislamiento y la fiebre de la cabina, un aspecto ha sido especialmente extraño, porque es el tipo de pensamiento que te hace sentir como una persona terrible 
                simplemente por pensar eso, ¿podría haber algo sobre el bloqueo que hemos sufrido, frente a una pandemia letal y destructora de la economía, que en realidad ha sido ... algo bueno? Permíteme explicarte, no soy un psicópata. Es cierto que algunas almas introvertidas afortunadas 
                podrían haber descubierto que cero contacto social les conviene. O quizás perteneces a esa tribu casi mítica (y profundamente irritante) que realmente logró usar la cuarentena para iniciar un negocio, renovar el piso o aprender portugués. Sin embargo, para la mayoría de nosotros, 
                es más exacto decir que hemos aprendido algo importante sobre lo que realmente importa, a menudo de una manera agridulce. Ahora que están ausentes, hemos llegado a comprender cuánto significan para nosotros amigos o parientes específicos. O nos hemos dado cuenta, al aplaudir a los 
                trabajadores de la salud, de que tenemos mucho que agradecer, es solo que antes estábamos demasiado ocupados para notarlo. O nos damos cuenta de que nuestra vida social frenética, ahora en pausa, funcionaba para distraernos de ciertas preguntas esenciales pero incómodas sobre 
                nuestra carrera o relación, que ya no se pueden evitar. En este sentido, existe un paralelismo entre la pandemia y la forma en que algunas personas reaccionan al recibir un diagnóstico de cáncer. Nunca lo elegirías, por supuesto. Pero es una oportunidad profundamente significativa 
                para reevaluar cómo estás pasando tu vida: preguntar, en palabras del psicoterapeuta Dr. James Hollis, si está siguiendo un camino que lo agrande en lugar de disminuirlo. Lo que lleva a la siguiente pregunta obvia: a medida que comenzamos, lentamente, a recuperar algo parecido a la 
                normalidad, ¿podemos hacer algo para aferrarnos a alguna de estas lecciones, para construir una vida que sea mejor como resultado de lo que aprendimos durante el cierre de emergencia? El desafío es tanto colectivo como personal: el tipo de sociedad que queremos crear y el tipo de vida
                individual que pretendemos llevar. "Cuando la crisis disminuya", señala el economista radical Charles Eisenstein, "podríamos tener la oportunidad de preguntarnos si queremos volver a la normalidad, o si podría haber algo que hayamos visto durante este descanso en las rutinas que queremos 
                para traer al futuro ". O, como lo expresó el escritor y director con sede en Nueva York, el Sr. Julio Vincent Gambuto: "Llegamos a Marie Kondo en la mierda de todo". Inspeccionamos cada aspecto de nuestras vidas, cada amistad, cada gasto, lo que hacemos con nuestro tiempo libre, y nos preguntamos 
                si realmente queremos conservarlo o si es hora de deshacerse de él. Es notoriamente difícil convertir este tipo de epifanía en una acción a largo plazo. (Una vez que se recuperan del cáncer, las personas que juraron que vivirían una vida diferente con frecuencia no lo hacen). Pero una 
                técnica que es útil es llevar un diario: registrar tus ideas por escrito les da una vida propia, más allá de las fugaces emociones que las generaron, por lo que es más probable que las integres en tu vida. Las reglas generales memorables también pueden ayudar. Una cosa es resolver, de alguna manera 
                vaga, hacer mejores contactos con amigos que necesitan apoyo después de la cuarentena; pero es otra seguir la recomendación de Movember, la organización benéfica de salud de los hombres, y memorizar el acrónimo "Alec": preguntar (cómo le va a alguien); escuchar (sin juicio); fomentar la acción 
                (sugerir cosas simples que podrían ayudar) y mantenerse en contacto (sugerir tener otra conversación pronto). Y, sin embargo, el legado más perdurable de estos tiempos puede ser simplemente haberlo superado. Los adultos más jóvenes de hoy en día a menudo son acusados ​​de ser emocionalmente frágiles,
                y se ha argumentado que la pandemia podría empeorar las cosas; que nos convertiremos en una sociedad de personas que entran en pánico cada vez que alguien tose en el autobús o un corredor pasa a menos de dos metros. Sin embargo, la investigación sobre el "crecimiento postraumático" sugiere que podría
                suceder lo contrario. Millones de personas, en lugar de pasar por la vida tenuemente temiendo alguna calamidad, habrán experimentado una muy real y sobrevivieron. Darse cuenta de que superar la tormenta es aprender algo nuevo y alentador sobre tus capacidades de resistencia. Neutraliza el miedo.
                Si puedes lidiar con eso, tienes muchas menos razones para seguir evitando cualquier elección de carrera o relación aterradora pero satisfactoria que hayas estado atrasando antes. Ese es el verdadero sentido en el que los eventos surrealistas del 2020 pueden demostrar, al final, haber contenido 
                las semillas de la oportunidad. No porque se suponía que debías usar tu tiempo en casa para lanzar un ajetreo lateral, o aprender una nueva habilidad impresionante, sino porque saldrás de la experiencia con un sentido más claro de lo que importa en la vida y más confianza en ti para perseguirlo.`,
                img_1: '',
                text_2: '',
                img_2: '',
                number: 2
            },
            {
                img: './assets/img/articulos/art_03/img-noticias.png',
                title: 'Cómo explotar la burbuja de aislamiento y comenzar a reconectarse',
                caption: 'Te compartimos este artículo de Mr. Porter, que puede ser de gran ayuda en esta cuarentena. Midas convirtió todo lo que tocaba en oro.',
                text_1: `Te compartimos este artículo de Mr. Porter, que puede ser de gran ayuda en esta cuarentena. Midas convirtió todo lo que tocaba en oro. Desafortunadamente, el pobre rey no se molestó en pensar en las implicaciones de esta habilidad milagrosa, dándose cuenta demasiado tarde de que incluso su comida se convertiría en metal precioso en el momento en que la tuviera en sus manos. ¿La moraleja de esta historia? Tener cuidado con lo que deseas. ¿Levantarte cuando quieras? Listo. ¿Descansar en tus pijamas Derek Rose todo el día? Listo. ¿Pasar un martes viendo los episodios de Star Wars del uno al nueve mientras masticas una caja entera de cereal? Listo. Sin embargo, ahora, meses después del cierre de emergencia, la novedad de nuestra nueva situación ha comenzado a desaparecer. Separado de amigos y familiares, quedando sin ideas para las cenas, ¡no papas fritas al horno otra vez! - y al mismo tiempo conscientes de una amenaza potencial para nuestra propia salud y la de nuestros seres queridos que zumban como un ruido blanco en el fondo, nos encontramos volviéndonos apáticos, lentos y, sobre todo, aburridos. Para aquellos de nosotros que vivimos solos, estas dificultades se ven agravadas por sentimientos de aislamiento intenso. La soledad es una emoción particularmente difícil para los hombres, porque puede sonar como debilidad o incluso algo vergonzoso. ¿Qué, no amigos? ¿Qué sucede contigo? Un estudio en la Universidad de Waterloo reveló que los hombres no tienen más probabilidades de sentirse solos que las mujeres, pero les resulta mucho más difícil admitirlo debido a su asociación con la vulnerabilidad, que es algo que tradicionalmente se ha alentado a los hombres a suprimir. Mi cliente, Zak, comenzó a trabajar desde casa hace seis semanas. Inmediatamente duplicó sus sesiones conmigo a dos veces por semana. "Ya me está haciendo sentir solo", dijo en ese momento, un sentimiento que afortunadamente pudo revelarme después de la confianza entre nosotros construida durante dos años de trabajar juntos. "Sí, todavía estoy ocupado con el trabajo, pero solo ver gente en Zoom para reuniones sin diversión o bromas realmente me está frustrando". Estaba feliz de ayudar a Zak, pero pronto llegó un alivio inesperado y bienvenido. Al darse cuenta de que su edificio de apartamentos ahora estaba lleno de personas solitarias como él, mi cliente comenzó a sentirse menos solo. Durante un ritual semanal de aplausos para el NHS, llamó desde su balcón a sus vecinos, lleno de valentía por su aislamiento compartido. Avanza dos semanas, Zak ahora tiene chats semanales de Zoom con tres de sus amigos vecinos e incluso han comenzado a ayudarse con el supermercado. Mi cliente ha encontrado una comunidad de apoyo en su puerta, por lo que rápidamente regresó a nuestras sesiones regulares de una vez por semana. Se siente extraño escribir esto en un momento en que muchos ven sus vidas impactadas negativamente, pero nunca he visto a Zak más feliz desde que encontró esta comunidad de vecindario para sí mismo, un grupo de amigos que imagino que aún se mantendrá cerca una vez que se levanta el bloqueo. Los sentimientos de aislamiento han estado arrasando a los hombres desde mucho antes de que esta pandemia pusiera nuestras vidas patas arriba. Es un problema especialmente agudo en nuestra vida profesional. Una corporación de noticias y medios de comunicación donde dirigía talleres de capacitación en liderazgo estaba tan preocupada por esto que su equipo de recursos humanos me pidió que desarrollara un programa para ayudar a los hombres a "reclinarse" y conectarse más con sus compañeros de trabajo. Les preocupaba que los empleados varones de la compañía se retiraran de las interacciones sociales, lo que estaba teniendo un impacto negativo en la moral y la productividad. Irónicamente, el aislamiento que Zak sentía mientras trabajaba desde casa también puede ocurrir fácilmente en una oficina abierta de planta abierta. Ocurre cuando usamos auriculares en nuestros escritorios, o cuando tenemos reuniones virtuales con un colega que se sienta a 10 segundos de distancia. Todas estas cosas pueden parecer la opción fácil. Pero tener conexiones significativas con los demás reduce nuestros niveles de estrés, hace que nuestros problemas parezcan más pequeños y al mismo tiempo nos da un poderoso sentido de pertenencia, que puede ser beneficioso no solo para nosotros sino también para nuestros lugares de trabajo y comunidades. Sin estas conexiones significativas, al igual que el Rey Midas, podemos comenzar a sentirnos rápidamente hambrientos. Pero como lo ilustra la historia de mi cliente Zak, incluso en un momento como este, el potencial para la amistad humana es más abundante que el oro, y también mucho más valioso.`,
                img_1: '',
                text_2: '',
                img_2: '',
                number: 3
            },
            {
                img: './assets/img/articulos/art_04/Artículo-PS-sept_foto.png',
                title: 'Tips para volver a salir con niños',
                caption: 'Después de un largo tiempo de estar en casa, disfrutar el tiempo en familia y cuidarnos en los momentos más importantes, llegó el momento de salir de nuevo y volver a disfrutar de cada paseo.',
                text_1: `Después de un largo tiempo de estar en casa, disfrutar el tiempo en familia y cuidarnos en los momentos más importantes, llegó el momento de salir de nuevo y volver a disfrutar de cada paseo con los más pequeños del hogar, sin olvidar todos los cuidados necesarios para una nueva experiencia, segura.

                Estos fabulosos tips te ayudarán a cuidar a los más pequeños fuera de casa.`,
                img_1: './assets/img/articulos/art_04/WhatsApp-Image.jpeg',
                text_2: `
                1. ¡Lleva contigo alcohol en gel! Este será el primer tip. Aunque la mayoría de establecimientos cuentan con un dispensador de gel, es importante que lleves uno siempre contigo y desinfectes las manos de los pequeños siempre que sea necesario, antes y después de comer alguna golosina o descansar en algún lugar.
                
                2. ¡Mascarillas de repuesto! Los papás sabemos que hay pequeños que se rehúsan a utilizar mascarillas y en cualquier momento pueden perderla. Es necesario que llevemos un repuesto para ellos si en algún momento la pierdan o la ensucian. La limpieza de las mismas siempre será un factor súper importante.
                
                3. ¡Mantén el distanciamiento físico! Los niños por naturaleza aman conversar y jugar con otros niños. Es importante hablar con tus pequeños y explicarles la situación. Ya que es primordial mantener el distanciamiento social, así evitar algún tipo de contagio.
                
                4. Regreso seguro… No olvides que, al llegar a casa, deberás desinfectar los zapatos, lavar sus manos y si es posible un cambio de ropa. Esto ayudará a mantener seguros a tus pequeños después de un feliz paseo.`,
                img_2: './assets/img/articulos/art_04/WhatsApp-Image-2.jpeg',
                number: 4
            },
            {
                img: './assets/img/articulos/art_05/Artículo_MF_Restaurantes.png',
                title: 'Marcas guatemaltecas que debes visitar en nuestros Centros Comerciales',
                caption: '¡Te extrañamos! Nuestras puertas abren de nuevo para recibirte con lo mejor, tus tiendas favoritas y restaurantes preferidos están de vuelta para que puedas comenzar a crear momentos inolvidables.',
                text_1: `¡Te extrañamos! Nuestras puertas abren de nuevo para recibirte con lo mejor, tus tiendas favoritas y restaurantes preferidos están de vuelta para que puedas comenzar a crear momentos inolvidables, disfrutar de las tendencias en moda y de sabores únicos, siempre tomando las medidas de seguridad e higiene que desde ya estamos poniendo en práctica.

                Nuestro estilo de vida se ha transformado con un nuevo comienzo y adaptándonos a una nueva normalidad. Ahora queremos cuidar de ti y tu familia más que nunca, para que te sientas tranquilo al momento de visitarnos. Nos entusiasma volver a ser parte de tus experiencias, acercándonos a ti de forma segura.
                
                Para ninguna persona es secreto que cualquier industria se ha transformado. Septiembre es el mes perfecto para apoyar lo nuestro, además de poder visitar las tiendas en nuestro centro comercial puedes encontrar algunas de estas marcas dentro de nuestro innovador sistema Shop&Collect una gran herramienta que puedes utilizar para realizar tus compras sin necesidad de bajar de tu vehículo. Te compartimos algunas de nuestras tiendas de consumo local para que conozcas los beneficios que actualmente tienen.
                
                Moda y accesorios
                La moda es algo que está en constante cambio, presentando nuevas tendencias con las que podemos conectar. La comodidad y versatilidad ahora son los protagonistas, acompañado de un toque minimalista con el que podemos encontrar un equilibrio de funcionalidad y estilo.
                
                Vivir la experiencia Saúl es algo único. Saúl E. Méndez regresa con las mejores ofertas y nueva colección inspirada en la comodidad y estilo. Puedes encontrarla en el nivel 1 o visitar su tienda en línea https://tienda.saulemendez.com/.`,
                img_1: './assets/img/articulos/art_05/PhotoshootMF__49.jpg',
                text_2: `Emporium no se detiene, además de poder disfrutar del confort y frescura de sus prendas, puedes obtener 20% de descuento en productos seleccionados al realizar tus compras online. Además podrás encontrar la nueva colección de Emporium en colaboración con Korbata en la que trece emprendedores chapines de diferentes giros de negocio aportaron divertidas ideas para elaborar 936 corbatas, corbatines y billeteras. ¡Te encantará tanto como a nosotros! www.emporium.com.gt/shop

                Distefano se caracteriza por ser una marca guatemalteca que presenta tendencias juveniles en ropa femenina y masculina. Outfits y looks totalmente únicos, cómodos y divertidos, ideales para ti. Encuéntrala en el nivel 1 o realiza tus compras por medio de Shop&Collect.

                Con los new arrivals de Kami definitivamente, ¡quedarás enamorada! Podrás combinar outfits súper frescos y modernos. Encuéntralos en tienda ubicada en nivel 2 o utiliza nuestro sistema Shop&Collect para hacer tus pedidos.

                Wakami es una marca de accesorios de moda tejidos a mano por mujeres guatemaltecas, están diseñados para inspirar a aquellos que sueñan con un mundo mejor. Ahora, puedes disfrutar de tus increíbles accesorios comprando en línea aquí https://bit.ly/flash-sale-new-world 
                o visitarla en el nivel 1.
                
                Electrónicos

                Lo último de tecnología lo encuentras primero en Max Distelsa. Ahora puedes realizar tus compras desde WhatsApp: https://wa.link/gfgtv0 y aprovecha los beneficios en las mejores marcas de electrónicos.
                
                Cafés
                
                Todos sabemos que podemos describir al café de Guatemala como Ú-N-I-C-O en el mundo, ¡y eso nos encanta! Además de poder visitar &Cafe y Café Barista, podrás utilizar aplicaciones como Hugo, UberEats, Glovo y WhatsApp para hacer tu pedido y recibirlos en casa.`,
                img_2: './assets/img/articulos/art_05/Artículo_MF_Cafes.png',
                text_3 : `Librerías y Hogar

                La nueva colección Septiembre-Octubre de Cantel representa nuestra cultura por medio de elementos de mantelería, toallas, limpiadores, platos y más. Te sorprenderá tanto que vas a querer tenerla toda en tu hogar. Puedes hacer tus pedidos por medio de WhatsApp 4115-2742
                
                Artemis Edinter se reinventa cada día con historias nuevas para compartir, libros que te inspirarán a crear o descubrir. Si eres un lector por naturaleza, podrás utilizar el beneficio de Shop&Collect y comprar todo lo que necesitas.
                
                Restaurantes de Tenedor
                San Martín y Skillets están grabados en nuestros corazones, nos llena de alegría poder ver sus puertas abiertas nuevamente y poder invitarte a disfrutar de sus sabores de independencia, sabores que nos recuerdan cuánto amamos a Guatemala. Puedes disfrutarlos en restaurantes o realizar tu pedido a domicilio.

                Ahora, prepárate y disfruta de las recomendaciones que te hemos dado de estas increíbles tiendas, ¡ya no esperes más! Esperamos volver a verte pronto. #JuntosSaliendoAdelante #SupportLocal Te compartimos el link de Shop&Collect para que conozcas nuestras tiendas participantes: https://shopandcollect.spectrum.com.gt/`,
                img_3: './assets/img/articulos/art_05/Artículo_MF_Tecnología.png',
                number: 5
            },
            {
                img: './assets/img/articulos/art_06/PhotoshootMF__9.jpg',
                title: '¿Nos extrañaste? ¡Estamos listos para recibirte!',
                caption: 'Sabemos que te cuidaste y te seguirás cuidando el tiempo necesario, para proteger a los que más quieres, por ello estamos listos para recibirte.',
                text_1: `¿Una tarde de helado en familia? ¿Las tardes de café con las amigas? ¿la cita en el cine? ¿el día de compras de mamá e hija? ¿Qué es lo que más extrañaste durante todo este tiempo?

                Sabemos que te cuidaste y te seguirás cuidando el tiempo necesario, para proteger a los que más quieres, por ello estamos listos para recibirte.`,
                img_1: './assets/img/articulos/art_06/PhotoshootMF__112.jpg',
                text_2: `Estamos listos para volver a tener todos los momentos que más extrañaste, llenos de fuerzas y sorpresas para compartir contigo.

                Cuentan que una mirada, dice más que mil palabras…Y lo hemos comprobado. Las mascarillas serán necesarias por un largo tiempo, pero así nos mantendremos seguros y saldremos adelante.
                
                Volveremos a recordar la época del colegio, donde el lavado y aseo de manos era muy frecuente. Por ello, es necesario que te cuides y te apliques alcohol en gel durante tu visita.
            
                Cuidarnos es cuidarte. Estamos listos para recibirte, con todas las medidas necesarias, para que te sientas seguro, para que te sientas como en casa. Te esperamos.`,
                img_2: './assets/img/articulos/art_06/PhotoshootMF__135.jpg',
                number: 6
            },
            {
                img: './assets/img/articulos/art_07/OM_SEP_ARTI╠üCULO_Thumbnail.png',
                title: 'Tres nuevas formas de entretenimiento',
                caption: 'La pandemia ha impactado de gran manera sobre las diferentes formas de entretenimiento que solíamos conocer; con viajes cancelados, conciertos y fashion shows.',
                text_1: `La pandemia ha impactado de gran manera sobre las diferentes formas de entretenimiento que solíamos conocer; con viajes cancelados, conciertos y fashion shows. 
                Nuestros hábitos han cambiado, y gracias a esto hemos encontrado muchas nuevas formas de entretenernos que probablemente no habíamos considerado antes pero ahora que las 
                conocemos, se quedan: 
                
                Diversión en casa

                Seguramente, antes no teníamos tanto tiempo para estar en casa y disfrutar de actividades sin tener que salir. Gracias a la importancia de cuidarnos y quedarnos en casa, descubrimos que 
                somos fans de algún juego de mesa, videojuego o disfrutamos de ver películas en familia. A pesar de que ahora podemos salir más, nos seguimos cuidando y visitamos los centros comerciales para 
                encontrar un nuevo juego de mesa para volvernos fans, una nueva televisión para seguir viendo las mejores películas en familia o juguetes para sorprender y entretenernos con los pequeños.`,
                img_1: './assets/img/articulos/art_07/OM_SEP_ARTI╠üCULO_foto 02.png',
                text_2: `Desarrollar los talentos que tenías escondidos

                Muchos otros, descubrieron su interés por la cocina, pintura, dibujo, lectura o la música. Y ahora no es solo una distracción de cuarentena, sino un nuevo hábito que disfrutamos hacer. Ahora vamos a De Museo 
                por nuestro nuevo libro, a Dominga por los marcadores y libretas para seguir en el dibujo, pintura y lettering. Y a Woodstock, por los accesorios de nuestros instrumentos para los nuevos amantes de la música. 
                ¿Qué otras cositas encuentras en tu tienda favorita, para desarrollar tu nuevo talento?`,
                img_2: './assets/img/articulos/art_07/OM_SEPARTI╠üCULO_foto 03.png',
                text_3: `Nuevas rutinas de ejercicio

                Muchos descubrimos rutinas de ejercicio, nos inscribimos a los programas de Femme Fit para no perder el hábito y la resistencia física, o empezamos a salir a correr durantes las mañanas. Probablemente otros, durante la 
                pandemia descubrieron su interés por el ejercicio. Ahora, podemos comprar nuestros artículos de ejercicio y accesorios en Shop & Collect, o nuestra ropa--con nuevas tallas--gracias a los resultados.`,
                img_3: './assets/img/articulos/art_07/OM_SEP_ARTI╠üCULO_foto 01.png',
                number: 7
            },
            {
                img: './assets/img/articulos/art_08/Arti╠üculo_NM-oct_Thumbnail.png',
                title: '¡Prepárate para un tour de sabores!',
                caption: '¡Todos estamos ansiosos por probar las delicias de la temporada!',
                text_1: `¡Todos estamos ansiosos por probar las delicias de la temporada! ¿Tienes alguna favorita? Acompáñanos en este tour y déjate llevar por el antojo que que estamos seguros provocaremos.

                Si eres fan de los baguettes o acompañamientos, San Martín será tu primera opción. En cada época del año, nos sorprende con un sabor de temporada diferente y seguramente para la época navideña, no será una excepción. ¡Mantente atento y disfrútalo en Portales, Naranjo Mall, Oakland Mall y Miraflores!`,
                img_1: './assets/img/articulos/art_08/Arti╠üculo_NM-oct_foto_01.png',
                text_2: `¡Carnívoro a la vista! Si los asados son lo tuyo, en Don Emiliano y La Estancia seguro te consentirán. Cortes y términos a tu gusto para que puedas disfrutar sólo o con toda la familia.

                Para los que aman los combos… Una opción completa, deliciosa y de temporada te espera en Skillets y Applebee's. Desde la ensalada familiar, hasta el combo de una deliciosa hamburguesa. ¡No te quedes con las ganas!
                
                Para terminar, con lo más dulce, los postres. Para los que amamos esta parte de la comida, sabemos que esta época es la mejor, desde cafés latte saborizados, hasta pasteles y crepas navideñas. Todo esto creado para consentirnos y hacernos disfrutar de las fiestas de fin de año.
                
                ¿Qué opción es tu favorita? ¡Recuerda que encuentras todo esto y más en tu centro comercial favorito!`,
                img_2: './assets/img/articulos/art_08/Arti╠üculo_NM-oct_foto_03.png',
                number: 8
            },
            {
                img: './assets/img/articulos/art_09/MF_Thumbnail.png',
                title: `¡Llegó el fin del ciclo escolar!`,
                caption: 'las clases en línea se terminan, las rutinas matutinas se detienen por unos meses y llegó el momento de realizar actividades entretenidas y divertidas',
                text_1: `Sabemos que estas vacaciones serán totalmente atípicas para los más pequeños, las clases en línea se terminan, las rutinas matutinas se detienen por unos meses y llegó el momento de realizar actividades entretenidas y divertidas en casa.

                Estos meses son perfectos para que exploren juntos las habilidades que pueden desarrollar tus hijos aprendiendo un nuevo idioma, ¿dónde? ¡La Academia Europea es la mejor opción! A partir de octubre inician clases virtuales para Juniors&Teens con edades entre 5 y 12 años. Contarán con tutorías personalizadas, más de 8 idiomas a elegir entre ellos inglés, francés, mandarín, alemán, el idioma que tu pequeño prefiera. Puedes obtener más información en el siguiente enlace https://clasesvirtuales.academiaeuropea.com/gt
                
                La música ayuda a estimular el desarrollo intelectual, auditivo, sensorial y motriz, además de aumentar las habilidades cognitivas de tus pequeños. En la Academia musical Woodstock encontrarás el programa de estimulación temprana music kids el cual se convertirá en una actividad fantástica para estas vacaciones.
                
                Y si buscas algo más personalizado, también cuentan con la opción de lecciones privadas donde el programa lo realizan en base a las preferencias musicales de tu hijo, ¡una opción genial! Ingresa aquí para obtener más información. http://www.woodstockgt.com/`,
                img_1: './assets/img/articulos/art_09/MF_Kasperle.png',
                text_2: `¿Buscas el libro perfecto para tu hijo?

                Si a tus hijos más grandes les apasiona la lectura y su imaginación no tiene límites, en Artemis Edinter y De Museo puedes encontrar historias asombrosas que te aseguramos les brindarán entretención positiva, desarrollando la concentración. El ejercicio de lectura estimula actividades sociales como la empatía, aumenta el vocabulario y los ayudará a convertirse en excelentes oradores. ¡Nos encanta esta opción! Puedes conocer los nuevos ingresos dando click en los siguientes enlaces https://www.artemisedinter.com/ y https://demuseo.com/`,
                img_2: './assets/img/articulos/art_09/MF_Libros.png',
                text_3: `Kits ToGo

                Uno de los mejores planes para los niños es realizar actividades que estimulan la psicomotricidad fina, la confianza y el trabajo en equipo. Ahora que el ciclo escolar terminó tus hijos tendrán más tiempo libre, por eso, ¡tenemos para ti opciones perfectas para las pequeñas mentes creativas!
                
                En nuestros centros comerciales, podrás encontrar puntos de venta con increíbles kits ToGo para que tus hijos los disfruten en casa. Dependiendo del día en que nos visites, podrás encontrar kits para decorar galletas, manualidades, un divertido kit para que tus pequeños puedan decorar sus mascarillas y muchos más. También puedes adquirirlos mientras visitas tu restaurante de Tenedor favorito, te aseguramos que a tus hijos les encantarán y tendrán un entretenimiento práctico y divertido.
                
                Sin duda, tenerte de vuelta y verte de nuevo en nuestros pasillos es algo que nos emociona. Hemos implementado las mejores medidas de protección e higiene para que durante tu visita te sientas tranquilo y cómodo. Para nosotros, tu salud es lo más importante, recuerda que #JuntosSaldremosAdelante ¡Te esperamos!`,
                img_3:'./assets/img/articulos/art_09/MF_KitsTOGO.png',
                number: 9
            },
            {
                img: './assets/img/articulos/art_10/OM_OCT_ARTI╠üCULO_thumbnail.png',
                title: '3 cosas que vas a disfrutar esta época navideña',
                caption: 'Durante este año un tanto atípico, donde recién hemos retomado nuestras rutinas, hemos visto a amigos, y hasta a familiares después de muchos meses.',
                text_1: `Durante este año un tanto atípico, donde recién hemos retomado nuestras rutinas, hemos visto a amigos, y hasta a familiares después de muchos meses. Comimos en nuestros restaurantes favoritos de nuevo, y visitamos los lugares que tanto extrañamos. Seguramente nos hemos preguntado también; ¿cómo será la navidad este año? Diferente, sin duda. Pero no hay excusa para que la época navideña en nuestros centros comerciales, sea tal y como siempre la disfrutas. ¿Quiéres conocer tres cosas que podrás disfrutar en esta época que tanto nos gusta a todos?
                
                Christmas photos!

                De las cosas que más disfrutamos--y atesoramos--cada navidad es la sesión de fotos navideña con toda la familia. Donde combinamos nuestros outfits, nos reímos, nos abrazamos y tomamos fotografías en spots diseñados para la temporada. Este año no será la excepción para disfrutar de este momento que se da una vez al año. Ahora podrás reservar espacio para tu sesión de fotos por medio de Spectrum APP, y online. Para que con todas las medidas de higiene y seguridad, puedas disfrutar de una de tus actividades favoritas de la época, con nosotros.
                `,
                img_1: './assets/img/articulos/art_10/OM_OCT_ARTI╠üCULO_foto_01.png',
                text_2: `Ofertas, ofertas, ofertas
                
                Todas las tiendas se están preparando con ofertas especiales durante la temporada, podrás venir a elegir el outfit navideño, de compras con el wishlist de los más pequeños y a consentirte un poco, porque la época también lo amerita. De igual manera, en cada una de las tiendas y en el centro comercial continuaremos con estrictas medidas para que te sientas totalmente cómodo al venir a realizar el 
                shopping que todos disfrutamos hacer; ¡shopping navideño!`,
                img_2: './assets/img/articulos/art_10/OM_OCT_ARTI╠üCULO_foto_02.png',
                text_3:`¡Siente el espíritu navideño!

                Por si te lo preguntabas, ¡sí tendremos a nuestro Santa! Sabemos que una parte emocionante de la época es poder pasar un momento con nuestro Santa, y los pequeños siempre han disfrutado de nuestra actividades, de las fotos o de un saludo de él. El centro comercial lucirá como todos los años, con nuestro árbol y decoración en todos los espacios para que sientas la navidad como cada año que nos visitas.`,
                img_3:'./assets/img/articulos/art_10/OM_OCT_ARTI╠üCULO_foto_03.png',
                number: 10
            },
            {
                img: './assets/img/articulos/art_11/Arti╠üculo_PS-oct_Thumbnail.png',
                title: '¿Cómo cumplir con tus metas en lo que resta del año? ¡Estos 3 tips te ayudarán!',
                caption: 'Estar en el peso ideal, hacer más ejercicio y cambiar de look, son algunos de los propósitos más comunes al iniciar el año, aunque los más difíciles de cumplir algunas veces.',
                text_1: `Estar en el peso ideal, hacer más ejercicio y cambiar de look, son algunos de los propósitos más comunes al iniciar el año, aunque los más difíciles de cumplir algunas veces.

                Con estos increíbles tips, lograrás cumplir estas metas… nosotros te ayudaremos.
                
                ¿No lograste bajar de peso?

                Una dieta balanceada, la moderación de azúcar y harinas te ayudará. Agrega a todo esto, vitaminas que ayudarán a que tu dieta y esfuerzo sea efectivo. En GNC encontrarás infinidad de opciones para cuidar tu peso y mantenerte saludable.`,
                img_1: './assets/img/articulos/art_11/Arti╠üculo_PS-oct_foto 01.png',
                text_2: `¡Más ejercicio!

                Para muchas personas, la pandemia sacó de ellas lo fit y el entrenador que llevaban adentro. Si este no fuera tu caso, en Sportline, Adidas, Puma y varios lugares de deportes, encontrarás artículos deportivos que te ayudarán a ser más activo. Como mancuernas, cintas de resistencia, alfombras y mucho más. ¡Aún estás a tiempo! Vamos, tú puedes.`,
                img_2: './assets/img/articulos/art_11/Arti╠üculo_PS-oct_foto 02.png',
                text_3:`El cambio de look tan esperado.

                Si fuiste de los que dijo, en verano me cambio el look, estos tips te ayudarán a darle un giro a tu imagen personal:
                
                a) ¡Un traje o vestido nuevo! Encuentra el outfit perfecto y del estilo que no hayas usado antes, esto te hará sentir mucho más seguro o segura contigo misma, apóyate de los asesores de venta para saber cuáles son esas prendas de temporada. Encuentra el tuyo en Emporium, Kami, Ovs o Forever21.
                
                b) ¡Un buen corte de cabello! Este será uno de los pasos más importantes para verte diferente. Busca un estilo distinto al que acostumbras, que vaya con tu personalidad y que esté en tendencia. Un buen corte de pelo siempre te hará lucir bien durante todo el año.`,
                img_3:'./assets/img/articulos/art_11/Arti╠üculo_PS-oct_foto 04.png',
                number: 11
            },
            {
                img: './assets/img/articulos/art_12/Articulo_PS-nov_Thumbnail.png',
                title: '¡Descubre los mejores regalos de Navidad para sorprender a esa persona especial!',
                caption: '¡Oficialmente la temporada navideña llegó! La Navidad es la mejor época del año y comenzamos a sentir el espíritu navideño y deseamos sorprender a quienes han formado parte de nuestra vida.',
                text_1: `¡Oficialmente la temporada navideña llegó! La Navidad es la mejor época del año y comenzamos a sentir el espíritu navideño y deseamos sorprender a quienes han formado parte de nuestra vida. Estas festividades son días especiales para compartir en familia momentos únicos y alegres. La víspera de Navidad es la noche más esperada, una noche llena de emociones en donde los más pequeños son quienes inician la cuenta regresiva para disfrutar de los obsequios que decoran el árbol.

                Los primeros días de Noviembre son ideales para iniciar a planificar tu lista de obsequios, además de ser los mejores días para aprovechar los descuentos, promociones y sales de medianoche, ¡tendrás más tiempo para poder disfrutar de la compañía, tus villancicos favoritos y la cena increíble de Nochebuena!. Es importante que tomes en cuenta la personalidad de cada miembro de tu familia o amistades para elegir el regalo perfecto, te compartimos algunas opciones increíbles que puedes encontrar en nuestro Centro Comercial.
                
                Los accesorios o prendas de moda siempre son una de las opciones favoritas por ser versátiles para todas las edades, lo mejor de todo es que puedes combinar accesorios pequeños y un bolso para formar un detalle único, optar por obsequiar blazers los cuales son una opción ligera y perfecta para usar en cualquier temporada del año y también tienes la opción de un par de zapatos clásicos y elegantes.`,
                img_1: './assets/img/articulos/art_12/Articulo_PS-nov_foto_01.png',
                text_2: `En Distefano encontrarás prendas para hombre y mujer, jackets, jeans, camisas y blusas de tendencia. Además podrás descubrir la nueva categoría de toallas de baño y playa con estilos modernos que lanzaron recientemente al mercado, un detalle que te aseguramos, ¡encantará!. Puedes vivir la experiencia de comprar en línea por medio de su App ingresando al siguiente enlace: http://onelink.to/26pkq4
                
                El cuero es una piel que no pasa de moda, se ha incorporado en el mundo de la moda por medio de prendas de vestir, bolsos, zapatos, accesorios y cinturones los cuales le brindan textura a cualquier look. Vélez cuenta con una gran variedad de artículos, puedes optar por obsequiar a papá un maletín portátil o una billetera y unos tacones o bolso elegante para mamá. Descúbre su nueva colección ingresando a 
                su página web, ¡te encantará todo lo que encontrarás! https://bit.ly/33I7hgd
                
                Si buscas accesorios o prendas más juveniles y modernas, en Jennyfer encontrarás una colección super cool con la que podrás sorprender a tus amigas o a quien tú quieras. También puedes adquirir una Jennyfer gift card, siempre se convertirá en un obsequio acertado ya que quien la recibe puede elegir a su gusto.`,
                img_2: './assets/img/articulos/art_12/Articulo_PS-nov_foto_02.png',
                text_3:`Las joyas son los regalos perfectos, busca una pieza que represente la personalidad, estilo o gustos de quien piensas al momento de elegir este tipo de obsequios. Las gargantillas, brazaletes, pendientes y anillos siempre son un detalle discreto y elegante. En Swarovski y Joyerías Villeda puedes encontrar una variedad increíble de piezas únicas con las que puedes deslumbrar a quien más amas.

                Si buscas un detalle que resalte, Watch It! cuenta con relojes de mano que completarán el outfit de la persona a quien le des este obsequio. Desde relojes minimalistas hasta smartwatches para hombres, mujeres y niños, ¡descubre los estilos visitando su página web! https://www.watchit.gt/
                
                Si de regalos tecnológicos hablamos, estos son una idea perfecta para regalar a alguien que le apasione la tecnología. Te compartimos algunas ideas:
                
                Videojuegos de tendencia, audífonos super prácticos, micrófonos, diversos estilos de drones aéreos, estos accesorios y gadgets de El Duende se convertirán en el obsequio perfecto. Visita su página web para hacer tus compras en línea. https://elduendemall.com/
                
                Si tienes en mente regalar artículos de línea blanca, televisores, electrodomésticos o electrónicos, Tiendas Max es el lugar ideal para hacerlo, puedes hacer tus compras en línea y obtener asesoría de venta remota para que no te compliques. Además, Tiendas Max te ofrece distintas formas de financiamiento para que puedas adquirir cualquier producto. Contáctalos por WhatsApp https://wa.link/w7o9s2

                Para los fanáticos de Apple, ¡hay de todo! IShop cuenta con los modelos más recientes de la marca y los accesorios que fascinan a todos. ¡Un regalo que te aseguramos, sorprenderá! Realiza tu compra en línea https://bit.ly/3iyu6qB

                Te aseguramos que con estas opciones podrás elegir el regalo perfecto para todos. Disfruta de tus compras navideñas y de la mejor época para compartir. ¡Te esperamos!`,
                img_3:'./assets/img/articulos/art_12/Articulo_PS-nov_foto_03.png',
                number: 12
            },
            {
                img: './assets/img/articulos/art_13/OM_NOV_ARTÍCULO_thumbnail.png',
                title: '4 elementos que deben estar en tus looks de otoño-invierno',
                caption: 'Podemos estar de acuerdo con que esta temporada es la favorita de muchas, no solo porque se acercan las fiestas, sino porque vienen las mejores tendencias del año.',
                text_1: `Podemos estar de acuerdo con que esta temporada es la favorita de muchas, no solo porque se acercan las fiestas, sino porque vienen las mejores tendencias del año y es hora de renovar nuestro guardarropa de acuerdo con todas las tendencias fashion de otoño invierno.
                
                Chaquetas de cuero

                Un elemento clave en la moda de todo los tiempos, y que nunca nos falla al crear un outfit de invierno. Clásico, pero siempre en tendencia para los días fríos, además de que agrega un toque chic a tu estilo y es tan versátil que puedes usarla con un look casual o semi-formal.`,
                img_1: './assets/img/articulos/art_13/OM_NOV_ARTÍCULO_foto_01.png',
                text_2: `Tonos naturales

                Una tendencia que ha permanecido desde hace algunos meses, y que seguramente no planea irse pronto. Un outfit monocromático con tonos naturales y suaves quedan bien en todas, y son fáciles de recrear. Prueba tonos beige o caramelo y estarás lista con esta tendencia.`,
                img_2: './assets/img/articulos/art_13/OM_NOV_ARTÍCULO_foto_02.png',
                text_3:`Colores brillantes en abrigos

                Uno más de un abrigo es esencial para esta temporada. Un color brillante, como el amarillo, combinado con tonos naturales o neutros dará un toque vibrante y hasta atrevido. Y además, es un elemento fácil de combinar con diferentes tipos de outfit. ¡Qué no te falte tu abrigo!
                
                Collares de cadena y argollas

                Y por supuesto, en tus outfits de invierno debes de contar con los accesorios en tendencia. Estos accesorios clásicos combinan perfecto con innumerables outfits de invierno, en tonos plata y oro. ¡Qué no te de miedo usarlos más de una vez a la semana! Combinan con todo, y le dan un plus a tu estilo.
                ¿Estás más que lista para crear los mejores outfits de temporada? Te esperamos en tus tiendas favoritas.`,
                img_3:'./assets/img/articulos/art_13/OM_NOV_ARTÍCULO_foto_04.png',
                number: 13
            },
            {
                img: './assets/img/articulos/art_14/MF_Articulo_Thumbnail.png',
                title: '¡El árbol navideño perfecto!',
                caption: 'Tradicional, colorido o elegante ¿Cuál opción será tu favorita? Si eres de los que quería poner su árbol navideño meses antes...',
                text_1: `Tradicional, colorido o elegante ¿Cuál opción será tu favorita?
                Si eres de los que quería poner su árbol navideño meses antes... Sabemos que amarás esta lectura. Si, llegó la época de lucirte con la decoración navideña ¿Ya tienes alguna en mente? Te compartimos algunas opciones en tendencia, que te encantarán.
                
                ¡Blanca Navidad!

                Sabemos que el blanco le dará un toque elegante a tu decoración, dando una sensación de frío alusivo al clima en la temporada. Añade tonos dorados y plateados para enmarcar detalles y listo. Las luces juegan un papel importante en esta decoración, ya que añaden el toque cálido para el hogar.`,
                img_1: './assets/img/articulos/art_14/MF_Articulo_01_RojoVerde.png',
                text_2: `Se dice que la Navidad tradicionalmente es roja y verde.

                ¡No siempre! Pero los que amamos la Navidad, puede ser de cualquier color. La decoración tradicional roja y verde, le da a tu hogar un toque cálido y hogareño, lleno de recuerdos de infancia, que harán que te transportes al pasado. Cabe mencionar, que hay artículos súper modernos en estos colores, así que si deseas mantener “los colores” de la Navidad estos serán tu mejor opción.
                
                ¡Colores por aquí y colores por allá!

                ¡Estos son los favoritos para los pequeños del hogar! Decoración Disney, colores alegres y no tradicionales, harán una decoración divertida y fuera de lo normal. Trata de no mezclar varios personajes o muchos colores, hacerlos de un tema en específico te ayudará a darle armonía y balance a tu árbol. De dos a tres colores, serán suficientes para una decoración bonita y divertida.`,
                img_2: './assets/img/articulos/art_14/MF_Articulo_02_Blanco.png',
                text_3:`¡Árboles monocromáticos!

                Rojos, azules, verdes o rosas. Son algunos de los nuevos colores para los árboles navideños utilizados durante los últimos años. Estos le dan un giro a tu decoración, dándote la oportunidad de decorar todo en un solo color. Estos son ideales para espacios luminosos o con poco color. Seguro con una opción de este tipo saldrás de lo tradicional y tendrás convertirás un ambiente de la casa en un espacio de alegría y luz.

                Sin importar cuánto sea tu presupuesto, estamos seguros que cualquiera de estas ideas se adecuarán no solo a tu gusto, sino también a que no gastes tanto y le des la bienvenida a esta navidad, que aunque atípica será un momento de compartir como siempre en familia. Entonces, ¿Cuál es tu opción favorita para decorar en este 2020 tu hogar?`,
                img_3:'./assets/img/articulos/art_14/MF_Articulo_04_Monocromatico.png',
                number: 14
            },
            {
                img: './assets/img/articulos/art_15/Artículo_NM-nov_Thumbnail.png',
                title: '¡La cena navideña, una noche para sorprender!',
                caption: 'La cena, los platillos y tips de decoración…Todo lo que necesitas en esa noche especial. La época más linda del año está a la vuelta de la esquina.',
                text_1: `La cena, los platillos y tips de decoración…Todo lo que necesitas en esa noche especial. La época más linda del año está a la vuelta de la esquina. Los que amamos esta fecha, sabemos que es momento de lucirnos con la cena navideña, la decoración y los regalos. Por ello, queremos compartirte algunos tips para una cena y decoración de mesa SENSACIONAL.
                
                ¿Cómo decorar la mesa?

                ¡No te estreses! Hay varias tiendas en dónde encontrarás muchísimas opciones para decorar tu mesa navideña, desde opciones tradicionales como el blanco y el dorado, como una Navidad colorida con azul y fucsia. Si buscas una decoración más sofisticada y con detalles únicos, opta por colores dorados o plateados, seguramente le darán el toque que buscas.
                
                ¡El platillo fuerte!

                Lo tradicional, siempre le encantará a tu familia, pavo relleno, pavo horneado o las variedades de preparación para un platillo exquisito, harán que toda tu familia quede sorprendida. Es muy importante, que adquieras productos frescos y de calidad.`,
                img_1: './assets/img/articulos/art_15/Artículo_NM-nov_foto 01.png',
                text_2: `Se dice que la Navidad tradicionalmente es roja y verde.

                ¡No siempre! Pero los que amamos la Navidad, puede ser de cualquier color. La decoración tradicional roja y verde, le da a tu hogar un toque cálido y hogareño, lleno de recuerdos de infancia, que harán que te transportes al pasado. Cabe mencionar, que hay artículos súper modernos en estos colores, así que si deseas mantener “los colores” de la Navidad estos serán tu mejor opción.
                
                ¡Colores por aquí y colores por allá!

                ¡Estos son los favoritos para los pequeños del hogar! Decoración Disney, colores alegres y no tradicionales, harán una decoración divertida y fuera de lo normal. Trata de no mezclar varios personajes o muchos colores, hacerlos de un tema en específico te ayudará a darle armonía y balance a tu árbol. De dos a tres colores, serán suficientes para una decoración bonita y divertida.`,
                img_2: './assets/img/articulos/art_15/Artículo_NM-nov_foto 02.png',
                text_3:`¡El acompañamiento perfecto!

                ¿Uno o dos? En esta época, es normal que todos queramos disfrutar un poco más, así que, si quieres preparar dos acompañamientos, no hay ningún problema. La tradicional ensalada de manzanas y almendras le dará un toque colorido a tu platillo. Acompañado de puré de papa será el complemento ideal.
                
                ¿Alguien dijo postre?

                ¡Si! Después de una deliciosa cena navideña, el postre no puede faltar. Encuentra una gran variedad de postres de la época en tu panadería o pastelería favorita.

                ¡Recuerda que lo más importante es estar en familia, agradecer y disfrutar de esa noche tan especial!`,
                img_3:'./assets/img/articulos/art_15/Artículo_NM-nov_foto 04.png',
                number: 15
            },
            {
                img: './assets/img/articulos/art_17/image.png',
                title: '¡Sorprende a los más pequeños en esta época navideña!',
                caption: '¡Llegó diciembre! Y con él, la Navidad, la época más esperada por todos, especialmente de los niños.',
                text_1: `¡Llegó diciembre! Y con él, la Navidad, la época más esperada por todos, especialmente de los niños. Nos encanta prepararnos para su llegada, el espíritu navideño despierta y comenzamos a disfrutar de los villancicos, decorar nuestro hogar, preparar recetas y las esperadas películas de la temporada.

                Navidad es una de las festividades preferidas de los niños, es una época perfecta para realizar planes divertidos, que aunque podemos hacerlos a lo largo del año, en Navidad son aún más especiales. Una manera para convivir en esta temporada es realizar actividades entretenidas y diferentes, pueden planificar en familia la visita a nuestro centro comercial donde tendrán experiencias inolvidables. Queremos compartir contigo 5 actividades que harán de esta Navidad una época especial para los más pequeños.
                
                1. Actividades en nuestros Centros Comerciales

                En nuestros talleres de pintura en cerámica y decoración de galletas navideñas podrás descubrir con tus pequeños nuevas habilidades, estas actividades se llevarán a cabo en Miraflores, visita nuestras redes sociales para conocer fechas, horarios y precios de cada kit para cada taller. También podrás crear tu propio helado de navidad con tus toppings favoritos y tomarte una fotografía con Santa. Por supuesto, debes tomar en cuenta la actividad más importante de la temporada: ¡escribir la carta a Santa!
                
                Oakland Mall sorprenderá a tus pequeños en “Santa's Little Helpers” donde podrán fabricar su juguete, de forma divertida y segura, ¡estarán ayudando a Santa esta Navidad!

                La inauguración de la temporada navideña viene acompañada de felicidad, por eso, en Naranjo Mall tenemos distintos talleres preparados para ti y tus hijos, podrán decorar donas, galletas y nuestros increíbles kits ToGo que podrás encontrar en las amenidades.

                Sabemos que en esta época te encanta escuchar villancicos, al visitar Portales podrás escuchar en cada rincón del mall los villancicos más populares de la temporada, al mismo tiempo, disfrutar en familia en el taller de bombas navideñas, un divertido taller de slime navideño y el taller de bola de cristal. Visita la página oficial de Portales para conocer fechas, horarios y precios de cada kit para cada taller.

                Además de estas actividades, en los cuatro Centros Comerciales podrán descubrir de forma divertida personajes navideños en “Santa's Secret Hunt” en donde deberán descifrar las 10 pistas que encontrarán en cada estación, al obtener los 10 personajes desbloquearán el árbol mágico y podrán participar por premios increíbles, ¡qué mágico!`,
                img_1: './assets/img/articulos/art_17/Artículo 1-dic_foto 02.png',
                text_2: `2. Leer cuentos de Navidad

                Si acostumbras leer cuentos cada noche con tus hijos, ¡esta actividad es ideal para ustedes! Las historias y los personajes navideños son los más icónicos y mágicos, además de pasar un tiempo agradable con tus pequeños, los cuentos, leyendas y libros navideños te harán recordar tu infancia. Encuentra los mejores libros de esta temporada en Artemis Edinter y De Museo, te compartimos sus páginas oficiales para que consultes disponibilidad: https://www.facebook.com/LibreriasArtemisEdinter/ y https://www.facebook.com/demuseo/`,
                img_2: './assets/img/articulos/art_17/Artículo 1-dic_foto 04.png',
                text_3:`3. Escribir felicitaciones navideñas o cartas de agradecimiento

                Este año fue un año totalmente fuera de lo común para todos, las visitas y reuniones familiares son menos recurrentes y extrañamos a nuestros seres queridos. Planifica una tarde con tus hijos para escribir cartas de agradecimiento, felicitaciones navideñas o pueden grabar un pequeño video utilizando disfraces con lo que tengan en casa y enviarlo el día de Nochebuena.
                
                
                4. Decorar el dormitorio
                
                ¡Decora junto a tus hijos sus dormitorios! La decoración navideña es una de las primeras actividades que realizamos en casa, utiliza luces de sus colores favoritos, adornos de sus personajes favoritos y vinilos decorativos, ¡será una habitación realmente acogedora!
                
                5. Hacer una corona de Navidad

                ¡Decora junto a tus hijos sus dormitorios! La decoración navideña es una de las primeras actividades que realizamos en casa, utiliza luces de sus colores favoritos, adornos de sus personajes favoritos y vinilos decorativos, ¡será una habitación realmente acogedora!

                ¡Esta actividad les encantará a tus hijos! Necesitarás: una sercha de alambre, pinzas de madera para ropa, listón rojo o el color que tus pequeños prefieran, pintura verde y un pincel.

                Dale forma redonda a la sercha de alambre, dejando el agarrador hacia arriba. Ayuda a tu pequeño forrando el alambre con el listón, pinten las pinzas de madera con la pintura verde, deja secar y pega una por una en el alambre hasta cubrir todo el círculo, por último haz una moña con el sobrante del listón y cuelga la corona en la puerta de la habitación o en la pared. ¡A tus pequeños les encantará agregar sus fotos favoritas!

                Disfruta en familia de estas actividades que te hemos recomendado. Nuestra familia te espera con la magia de la Navidad, ¡felices fiestas!`,
                img_3:'./assets/img/articulos/art_17/Artículo 1-dic_foto 05.png',
                number: 16   
            },
            {
                img: './assets/img/articulos/art_18/Artículo_2-dic_Thumbnail.png',
                title: '¡Top 5! Conoce 5 regalos que le encantarán a tu pareja',
                caption:'¡Se acerca ese día especial! Un día mágico para compartir y disfrutar con los que más quieres.',
                text_1: `¡Se acerca ese día especial! Un día mágico para compartir y disfrutar con los que más quieres.

                Sin embargo, escoger el regalo perfecto para ese ser tan especial que complementa tu vida con el que quieres compartir, no es siempre tan fácil. Así que te compartimos el top 5 de los regalos que le encantarán a tu pareja. ¡Continua con nosotros y conócelos!
                
                1. ¡Electrónicos!

                ¡A todos nos encantan estos regalos! Nunca está de más una secadora, una plancha de cabello, una rasuradora eléctrica o la tv que tanto soñó. ¡Hay muchísimas opciones que seguramente a tu pareja le encantarán! Nunca estará de más, llevarla al centro comercial y dejarla elegir su electrónico favorito.
                
                2. Un outfit perfecto

                ¡Siiiiiiiii! Tanto a las mujeres, como a los hombres, nos encanta la ropa, vestirnos bien y lucir genial en cada momento, así que un outfit o algún complemento le fascinará. Consulta cuál es su tienda favorita, seguro cada prenda del lugar se adaptará a su estilo y le encantará.`,
                img_1: './assets/img/articulos/art_18/Artículo_2-dic_foto 01.png',
                text_2: `3. ¡Por más zapatos!

                Sin pensarlo dos veces, esta es una gran opción, ya que no hay persona en el mundo, que no amé los zapatos. Hay opciones casuales, deportivas o elegantes. Seguramente conoces el estilo de tu pareja y sabrás cuáles son sus favoritos.
                
                
                4. ¡Los que no pueden faltar! Accesorios.
                
                Un anillo, un buen reloj, el collar que deseaba o la cadena a su medida, serán opciones para sorprender a tu pareja esta Navidad. Estos le darán siempre un toque elegante y chic a cualquier look.`,
                img_2: './assets/img/articulos/art_18/Artículo_2_dic_foto 02.png',
                text_3:`5. ¡Un certificado de regalo!

                Hoy en día, muchas parejas optan por una gift card de regalo, dejando que sus parejas, puedan elegir el regalo ideal. Esto para evitar algún tipo de cambio o gustos equivocados. Si conoces su tienda favorita, seguramente le gustará.
                
                Recuerda que lo más importante de la Navidad, y en especial esta que se aproxima pronto, no será únicamente el regalo o el valor del mismo, será también agradecer por lo que tenemos, compartir en familia y disfrutar cada momento juntos.`,
                img_3:'./assets/img/articulos/art_18/Artículo_2-dic_foto 05.png',
                number: 17
            },
            {
                img: './assets/img/articulos/art_19/RegalosJefe_Thumbnail.png',
                title: '¡5 ideas de regalo para quedar bien con tu jefe en esta Navidad!',
                caption:'¿Regalo para tu jefe? No te compliques ¡Nosotros sí sabemos que regalarle! Estas opciones que tenemos para ti, le encantarán y será tu “quedar bien”',
                text_1: `¿Regalo para tu jefe? No te compliques ¡Nosotros sí sabemos que regalarle! Estas opciones que tenemos para ti, le encantarán y será tu “quedar bien” con él o ella. ¡Conócelas!

                
                1. ¡Una botella de vino!


                Esto será una muy buena opción, para los amantes que les gusta el buen comer, acompañarlo con una buena botella te hará quedar muy bien. Consulta con anticipación cuál es su favorito o qué sabores de cepa le gustan más y sorpréndelo con este detalle. Dale un plus a tu obsequio, con algún maridaje para que lo disfrute al máximo.


                2. Un libro nunca falla.


                ¿Sabes cuál es su autor favorito? Seguramente en algún momento les recomendó algún libro o citó alguno. Busca contenido similar o sé extrovertido y compártele tu lectura favorita. Los libros siempre serán un regalo extraordinario, más si son del giro de negocio de la industria en la que trabajan.`,
                img_1: './assets/img/articulos/art_19/RegalosJefe_01_Vino.png',
                text_2: `3. ¡Decoración!


                No todos los jefes, aman tener algún tipo de decoración en su oficina, pero si el o la tuya es la excepción, un detalle para decorarla será genial. Alguna planta, un cuadro inspirador o una porta lapiceros, hará de tu obsequio algo muy especial. Recuerda acompañarlo de una linda nota, eso le dará un toque muy personal.
                
                
                4. ¡Una canasta especial!
                
                
                Puedes optar por crear tu canasta llena de productos variados. Si eres de los que conoce mucho a tu jefe, sabrás que es lo que prefiere, si es amante de un buen café, disfruta comer chocolate o lleva una vida super light. Sin duda esta opción será especial y le encantará. Hay opciones de canasta de productos comestibles o de cuidado personal, escoge de acuerdo a su personalidad.`,
                img_2: './assets/img/articulos/art_19/RegalosJefe_02_Libro.png',
                text_3:`5. Un vale de regalo.


                ¡Estos son los mejores! Ya que ellos podrán elegir su regalo y compraran lo que realmente necesitan. Un vale de ropa o accesorios para el hogar, será perfecto o por qué no alguna experiencia culinaria, siempre una buena cena es un regalo perfecto.
                
                Ahora que ya lo sabes, sorprende a tu jefe con un regalo súper especial para esta Navidad y nutre esa relación con un detalle que te hará distinguirte y que puedas compartir con él o con ella también en esta época navideña.`,
                img_3:'./assets/img/articulos/art_19/RegalosJefe_05_ValeRegalo.png',
                number: 18
            },
            {
                img: './assets/img/articulos/art_20/4_DIC_ARTÍCULO_thumbnail.png',
                title: '3 formas de celebrar tus reuniones navideñas con medidas de seguridad',
                caption:'Mientras que la pandemia nos mantuvo por mucho tiempo en casa, diciembre es uno de nuestros meses favoritos porque comemos delicioso.',
                text_1: `Mientras que la pandemia nos mantuvo por mucho tiempo en casa, diciembre es uno de nuestros meses favoritos porque comemos delicioso, celebramos las fiestas de fin de año y organizamos reuniones con nuestros diferentes grupos de amigos, colaboradores y familia, y como los las fiestas navideñas son una vez al año, ¡no se pueden cancelar! Te contamos puedes hacer esta época especial, con todas las medidas de seguridad:
                
                ¡Elige tu lugar!

                Es obvio, tienes que escoger el mejor menú para celebrar tu reunión navideña, pero, ¡no te guíes solo por el menú, o el precio! Es importante que el lugar donde celebres siga todas las medidas de seguridad para que tú y el resto de los invitados se sientan cómodos celebrando. Además, escoger un espacio al aire libre hace que el ambiente sea más acogedor, y disminuye el riesgo de propagación del virus. ¡Todos nuestros restaurantes cumplen con todas las medidas de seguridad para tu tranquilidad!`,
                img_1: './assets/img/articulos/art_20/4_DIC ARTÍCULO_foto 01.png',
                text_2: `Pon tus reglas

                Además de las medidas que el restaurante pueda tener, tus amigos/familia deben seguir ciertas medidas propias para mantener seguro al grupo; como lavarse las manos constantemente, usar mascarilla cuando no están comiendo y mantener cierto distanciamiento. Lo sabemos, quisiéramos abrazar a tooodo nuestro grupo, pero evita el contacto físico innecesario. Recuerda también contar con un dispositivo de alcohol en alguna de las mesas.`,
                img_2: './assets/img/articulos/art_20/4_DIC ARTÍCULO_foto 02.png',
                text_3:`¡Qué no falten los detalles!

                Los intercambios de regalos siempre le dan un toque especial a las reuniones navideñas, pero, ¿qué tal si este año hacemos algo diferente? En tu reunión con familia, amigos o compañeros de trabajo, después de disfrutar de comida, pláticas y risas, pueden alegrar la temporada navideña de alguien, con cosas que necesiten. ¡Elijan una causa! Y que no falten los regalos para personas que se pueden ayudar, en esta época de compartir.
                
                
                ¡Y listo! Puedes celebrar y tener el espíritu navideño de todos los años, estar seguro y compartir un buen momento con gente que lo necesita.`,
                img_3:'./assets/img/articulos/art_20/4_DIC ARTÍCULO_foto 03.png',
                number: 19
            },
            {
                img: './assets/img/articulos/art_21/2021_SALUDABLE_ARTÍCULO_thumbnail.png',
                title: '¡Por un 2021 saludable!.',
                caption:'Como todos los años, este 2021 vendrá cargado de oportunidades y de decisiones importantes que debemos realizar para cumplir las metas que nos trazamos.',
                text_1: `Como todos los años, este 2021 vendrá cargado de oportunidades y de decisiones importantes que debemos realizar para cumplir las metas que nos trazamos. Aquí te compartimos un poco de todo lo que debes tomar en cuenta para que este año siga siendo uno donde te pongas tú en el centro y que puedas realmente encontrar ese balance también. Ejercicio, dietas, vitaminas y todo lo que necesitas para iniciar este nuevo año al 100%.
                
                1. Agua, mucha agua.

                Esto será lo primero en tu lista de cuidados. Tomar agua es uno de los hábitos más recomendados por expertos en salud. Esto le ayudará a tu cuerpo a no comer de más, cuidar tu piel y mantenerte saludable. Si necesitas una motivación, recuerda contar con un recipiente súper lindo y que te ayude a medir la ingesta, te ayudará a lograr tu objetivo que mínimo deberá de ser de 2 litros al día.`,
                img_1: './assets/img/articulos/art_21/2021_SALUDABLE_ARTÍCULO_foto 01.png',
                text_2: `2. ¡Ejercicio en casa!

                Si optaste por realizar tu rutina en casa, es importante que tengas todos los accesorios necesarios para cumplir tu objetivo. No hay mejor motivación que adquirir tus pesas favoritas, tu alfombra del color que desees y todo lo que hará que logres terminar tu rutina con éxito.
                
                
                
                3. ¡No olvides las vitaminas!
                
                Adicional a dormir bien, 4 a 5 tiempos de comida, realizar ejercicio y de más, son importantes para estar saludable, pero ¿Necesitas más? Si, tomar vitaminas, suplementos entre otros les darán un plus a tus cuidados naturales, ayudando a el buen funcionamiento de tu organismo, mantenerte activo y con todas las energías que necesitas para tus rutinas. En GNC cuentan con todo lo que necesitas para iniciar este 2021 al 100%.
                `,
                img_2: './assets/img/articulos/art_21/2021_SALUDABLE_ARTÍCULO_foto 02.png',
                text_3:`4. ¡La ropa adecuada!

                Y, por último, pero no menos importante, la ropa. Utilizar ropa y calzado adecuado, será primordial al realizar tus rutinas diarias. Esto permitirá que te sientas cómodo y seguro en cada movimiento o actividad de tu agenda. Elegir tus prendas favoritas, será de motivación absoluta para terminar tu rutina con éxito.
                
                
                
                Entonces. ¿Estás listo para iniciar el 2021? Organízate, planea y haz estos tips parte de lo que te ayudará a cumplir tus objetivos. Recuerda siempre también que la actitud es el ingrediente que marcará también la diferencia ;)`,
                img_3:'./assets/img/articulos/art_21/2021_SALUDABLE_ARTÍCULO_foto 04.png',
                number: 20
            },
            {
                img: './assets/img/articulos/art_22/5Gadgets_Thumbnail.png',
                title: 'Nuevos comienzos, ¡nuevas oportunidades! Cinco gadgets para un increíble regreso a clases.',
                caption:'Este nuevo año viene con muchas oportunidades y aprendizajes. Los pequeños y adolescentes de la casa se preparan con mucho entusiasmo.',
                text_1: `Este nuevo año viene con muchas oportunidades y aprendizajes. Los pequeños y adolescentes de la casa se preparan con mucho entusiasmo para iniciar un nuevo ciclo escolar y probablemente ya estás evaluando algunas opciones para que su regreso a clases sea tranquilo, práctico y divertido.

                Durante la nueva normalidad la tecnología se ha convertido en nuestro mejor aliado, nos ha facilitado en grandes medidas la comunicación por medio de diversas plataformas y Apps. Además, nos permite simplificar múltiples tareas y actividades diarias. Las diversas modalidades escolares que se implementarán durante el ciclo 2021 vendrán con múltiples innovaciones, entre ellas, el uso de herramientas digitales y gadgets indispensables que se enfocarán en el desarrollo adecuado de los estudiantes.
                
                Te compartimos cinco gadgets súper útiles para tus hijos y dónde los puedes encontrar dentro de nuestro comercial, lo mejor de todo, ¡son ideales para cualquier grado académico!
                
                1. Laptop

                Una computadora portátil o laptop es la mejor herramienta que puedes brindarle a tu hijo para este regreso a clases, será el compañero fiel al momento de recibir clases en línea y trabajos escolares, otra gran ventaja es que además de poder utilizarla en casa podrá llevarla a todos lados, obteniendo un óptimo rendimiento escolar con batería de larga duración y un procesador eficiente.

                Puedes encontrar diversos equipos portátiles en iShop, la cual puedes encontrarla en el nivel 3 o visitar su tienda en línea https://ishop.gt/`,
                img_1: './assets/img/articulos/art_22/5Gadgets_01_Laptop.png',
                text_2: `2. Tablets

                La principal ventaja que verás en tus pequeños al brindarles un gadget multiusos es el interés por aprender y descubrir interactuando con diferentes plataformas dependiendo de la modalidad educativa que implementará el centro educativo de tu hijo. Las tablets aportan aprendizaje autónomo, reforzando la memoria visual de una manera entretenida.
                
                Puedes visitar El Duende, Max Distelsa, iShop y Electrónica Panamericana para elegir la que mejor se adapte a las habilidades y necesidades de tus hijos.
                
                
                3. Audífonos
                
                Este gadget nos acompaña a todos lados, cuentan con la ventaja de aislar el ruido del entorno lo cual los hace perfectos para obtener mayor concentración durante las clases en línea. Puedes encontrarlos en distintos tamaños, dependiendo de la edad de tu pequeño, juntos pueden elegir entre audífonos inalámbricos que suelen ser más cómodos o audífonos de mayor tamaño que tienen mejor calidad y definición de sonido. ¡Hay muchísimas opciones!
                
                Puedes encontrar la variedad disponible en Max Distelsa visitando su página oficial https://www.max.com.gt/audifonos o consultar disponibilidad en https://ishop.gt/`,
                img_2: './assets/img/articulos/art_22/5Gadgets_03_Audífonos.png',
                text_3:`4. Smart Watch

                Un excelente gadget para tus hijos adolescentes es un smartwatch. Podrán establecer recordatorios de proyectos importantes, exámenes y por supuesto, utilizar aplicaciones para medir su rendimiento físico durante la materia de deportes. ¡Es fantástico! Encuéntralos en tu tienda favorita de electrónicos dentro del centro comercial.
                
                
                5. Disco duro
                
                Los dispositivos de almacenamiento son indispensables en esta nueva era, te ayudan a organizar y compartir información de forma digital, son perfectos para conservar las tareas y proyectos de tus hijos de forma segura. Puedes encontrar discos duros de distintas capacidades de almacenamiento en Electrónica Panamericana. Visita su página oficial dando click aquí: https://electronicapanamericana.com/tecnologia/
                
                
                Ahora ya podrás elegir entre estas increíbles recomendaciones las mejores herramientas para el regreso a clases 2021. ¡A tus hijos les encantarán! Visítanos y encuentra todo lo que necesitas. ¡Te esperamos!`,
                img_3:'./assets/img/articulos/art_22/5Gadgets_05_DiscoDuro.png',
                number: 21
            },
            {
                img: './assets/img/articulos/art_23/Artículo-Ene-NuevaNormalidad_Thumbnail.png',
                title: '¡Que la nueva normalidad sea siempre verte fashion!',
                caption:'Después de este atípico 2020 sabemos que el nuevo accesorio a incluir en nuestro closet será la mascarilla, ¿quieres lucir perfecta o perfecto con cada outfit?',
                text_1: `Después de este atípico 2020 sabemos que el nuevo accesorio a incluir en nuestro closet será la mascarilla, ¿quieres lucir perfecta o perfecto con cada outfit? Estos tips te ayudarán.
                
                1. ¡Momento de combinar!

                Rojo, negro, azul, blanco o multicolor. Hay una gran variedad de colores de mascarillas, las cuales podrás combinar con tu outfit del día. Varios lugares de ropa de caballero, ofrecen camisas y mascarillas con el mismo diseño, dándole un plus a tu estilo.


                2. Con un toque elegante…

                Si tienes algún evento, puedes optar por mascarillas más elegantes, algunas opciones poseen brillos, perlas o prints iguales a tu vestimenta. Sin duda, estás opciones le darán a tu outfit un toque especial. También existen accesorios para sujetar desde tu cuello la mascarilla a donde vayas. Estos vienen de distintos materiales y tan elegantes como quieras lucir.`,
                img_1: './assets/img/articulos/art_23/Artículo-Ene-NuevaNormalidad_foto 01.png',
                text_2: `3. ¡La barba no será un problema!

                Si eres de los que utiliza barba, es ideal que utilices mascarilla con filtro, esta te permitirá una mejor ventilación y evitarás el calor innecesario. Encontrarás varios colores disponibles, para combinarlas con tu atuendo.
                
            
                4. ¡El maquillaje no puede faltar!
                
                Deja que tus ojos hablen, utilizando un maquillaje adecuado, la mascarilla no será un problema. Un delineado suave, corrector y sombras combinables con tu tono de piel, será una excelente opción.`,
                img_2: './assets/img/articulos/art_23/Artículo-Ene-NuevaNormalidad_foto 03.png',
                text_3:`5. ¿Accesorios para el cabello?

                ¡Si! Existe un mundo de accesorios muy lindos para el cabello, desde headbans y clips hasta collares colgantes. Todo esto le dará un toque único a tu outfit, recuerda que puedes combinar tu atuendo con tu mascarilla y darle un toque brillante en tu cabello.
                
                No sabemos aún hasta cuando la mascarilla será parte de nuestra normalidad, pero lo que sí sabemos es que con estos tips, la mascarilla no será un obstáculo para lucir genial a donde vayas.`,
                img_3:'./assets/img/articulos/art_23/Artículo-Ene-NuevaNormalidad_foto 04.png',
                number: 22
            },
            {
                img: './assets/img/articulos/art_24/Artículo-ene-metas2021_Thumbnail.png',
                title: 'Cómo cumplir tus metas 2021 en cinco pasos',
                caption:'Planear, y fijarnos metas nos emociona, empezar algo nuevo también. Pero, ¿qué tal si este año lo hacemos bien? ¡Te compartimos cinco formas para cumplir tus metas este 2021!',
                text_1: `Este año sin duda dejará cierta huella en todos nosotros, y podemos fácilmente rescatar muchas cosas positivas que seguramente aprendimos, vimos cosas desde otra perspectiva y el este nuevo año seguro no seremos los mismos, y tal vez, tampoco tengamos la misma forma de ver las cosas. Sin embargo, planear, y fijarnos metas nos emociona, empezar algo nuevo también. Pero, ¿qué tal si este año lo hacemos bien? ¡Te compartimos cinco formas para cumplir tus metas este 2021!
                
                Metas SMART

                Fijarnos metas nos impulsa a trabajar duro cuando empezamos un nuevo año, pero después de uno de tanto aprendizaje, lo haremos mejor. ¿Has escuchado hablar sobre la metodología SMART? Por sus siglas en inglés indica que tus metas deben ser específicas, medibles, realizables, realistas y oportunas. Cuando fijas tus metas con la metodología SMART, primero las identificas, crees en que puedes lograrlas y las apuntas, para después establecer métodos para lograrlas, mucho más fácil, ¿no?`,
                img_1: './assets/img/articulos/art_24/Artículo-ene-metas2021_foto 03.png',
                text_2: `¡No te apresures!

                Todo está cambiando muy rápido. No puedes comerte al mundo de una mordida, y tampoco debes apresurarte con tus metas. Después de que las defines bien, y sabes qué cosas debes hacer para cumplirlas, trabaja despacio, y no te apresures. Tus metas no se van a mover; tú te mueves para cumplirlas pero paso por paso, ¡tienes todo un año! Sé constante y trata de dar pequeños pasos que te ayuden a cumplir la gran meta que te trazaste.
                
                
                ¡Abraza el cambio!
                
                EL 2020 nos dejó que aunque planeemos y tengamos metas fijas, debemos también ser flexibles y saber que no tenemos el control de todo nuestro contexto. La pandemia nos vino a recordar una lección muy importante y es que la buena actitud frente al cambio siempre debe ser una constante. El cambio y ser flexible es parte en el cómo cumplir esas metas, pues al final toda meta también resulta en un cambio.`,
                img_2: './assets/img/articulos/art_24/Artículo-ene-metas2021_foto 01.png',
                text_3:`¡Adueñate de tu meta!

                Hay metas que solo llegan al 2 de enero porque ¡No son tus metas! Son de tus papás, amigos o pareja. Cuando te adueñas de tus metas y no permites que otras personas te hagan creer que sus metas son las tuyas, seguro las cumples. Y después de un año estarás satisfecho de que el 2021 no solo fue un nuevo comienzo, sino una oportunidad para hacer las cosas mejor y cumplir todo lo que tú te propusiste.
                
                 
                ¿Qué es lo que realmente te hace feliz?
                
                Al ponerte nuevas metas y saber cuáles son verdaderamente las tuyas, pregúntate: ¿Qué es lo que realmente te hace feliz? El 2020 nos dejó con la inquietud de cuestionarnos con quién y en qué estamos invirtiendo nuestra vida. Al responderte seguro tendrás que tomar decisiones difíciles que tomar, pero el seguir tu corazón debe de ser lo más importante que alguien puede hacer para encontrar y cultivar esa felicidad que año con año nos trazamos.
                
                  
                ¡Estamos listos para el 2021!`,

                img_3:'./assets/img/articulos/art_24/Artículo-ene-metas2021_foto 05.png',
                number: 23
            },
            {
                img: './assets/img/articulos/art_25/spectrum_thumbnail.png',
                title: 'Temporada Primavera | Verano 2021',
                caption:'La temporada primavera | verano 2021 fue diseñada en el primer confinamiento asi que la experiencia para los diseñadores fue totalmente unica.',
                text_1: `La temporada primavera | verano 2021 fue diseñada en el primer confinamiento asi que la experiencia para los diseñadores fue totalmente unica. Muchos de los desfiles fueron realizados al aire libre, haciendo que la naturaleza fuera uno de los puntos principales.

                De las tendencias que siguen estando son los crop tops, los pantalones oversized (Cynthia Rowley y Elie Saab), pattern blocks (Issey Miyake y Loewe)
                
                Para este verano el beachwear se enfoca en el tie-dye en piezas completas como lo muestra Bond Eye.`,
                img_1: './assets/img/articulos/art_25/DICE-KAYEK-SPRINGSUMMER-2021-LOOK-7.jpg',
                text_2: `El color block presentado por Dice Kayek y Prada con paletas de colores mas brillantes, evocando una sensacion de frescura.

                El look Venice Beach inspirado en los elementos Disco como lo mostraron las pasarelas de Isabel Marant y Balmain con paletas tornasoles y lentejuelas.`,
                img_2: './assets/img/articulos/art_25/ANNAKIKI-SPRINGSUMMER-2021-LOOK-37.jpg',
                text_3:`El uso de materiales de primera calidad y enfocado en la sostenibilidad son de los factores que se podran ver en esta temporada, como los cristales de swaroski.`,
                img_3:'./assets/img/articulos/art_25/ISABEL-MARANT-SPRINGSUMMER-2021-LOOK-8.jpg',
                number: 24
            },
            {
                img: './assets/img/articulos/art_26/Thumbnail_Tendencias_2021_Amaril.png',
                title: 'Tendencias 2021',
                caption:'La combinacion de colores para la primavera y verano 2021 es refrescante, inspiracional y alegre',
                text_1: `Los colores del 2021 son el PANTONE 13-0647 Illuminating y PANTONE 17-1504 Ultimate Gray. La combinacion de colores para la primavera y verano 2021 es refrescante, inspiracional y alegre.
                
                Se destacan los colores calidos como el Raspeberry Sorbet, Green Ash y Marigold. Esto tambien depende de como el 2021 se va desarrollando relacionado a la salud y la pandemia. Como cada pais maneje de manera estrategica la distribucion de la vacuna va a influenciar la paleta de colores con algo fresco, luminoso y esperanzador.`,
                img_1: './assets/img/articulos/art_26/spring-2021-trends-women-over-40-fountain0f30.jpg',
                text_2: `Las prendas de lana, que van desde las combinaciones monocromaticas, las mangas fruncidas y los patrones graficos son chic y comodo.

                Los colores pastel como color block o en combinacion es esencial para esta primavera como lo muestra Versace Resort y el Fashion Week en Copenhagen.`,
                img_2: './assets/img/articulos/art_26/fancy-fleece-spring-2021-trends-fountainof30.jpg',
                text_3:`Otra de las tendencias para esta primavera/verano son las botellas de agua que son el "new essential". Fendi y Givenchy mostraron opciones en metal, cuero y fibras naturales.

                Los colores pastel como color block o en combinacion es esencial para esta primavera como lo muestra Versace Resort y el Fashion Week en Copenhagen.
                
                Los flats y las pijamas de seda no solo son para dormir, son perfectos para el dia a dia y como beach cover ups para este verano asi como el uso de patrones super sized llenos de colores.`,
                img_3:'./assets/img/articulos/art_26/6eefbfba68cca3e078667745c16020ed.jpg',
                number: 25
            },
            {
                img: './assets/img/articulos/art_27/Artículo-abril-spectrum_foto03.png',
                title: 'Spectrum, ha demostrado que el éxito va de la mano con el medio ambiente',
                caption:'Con iniciativas uso responsable y eficiente de agua, el desarrollador inmobiliario ha consumido 980,000 metros cúbicos menos de este líquido, equivalente a 392 piscinas olímpicas',
                text_1: `Hoy en día, las empresas que quieren tener éxito no solo deben centrar sus esfuerzos en la rentabilidad económica, sino en acciones que tengan un impacto positivo en la protección del planeta y en la calidad de vida de las personas. Cumplir estándares de gobernanza ambiental es mandatorio en un mundo en el que los recursos naturales son cada vez más preciados y en donde la eficiencia y 
                la innovación son clave para gestionarlos. En esa gestión sostenible, cobra especial relevancia el agua, un recurso del cual está compuesto el 70% del cuerpo humano y el 70% de la Tierra.
                
                Enfocado en crear conexiones entre las personas y el espacio que les rodea para construir oportunidades en las que puedan alcanzar sus sueños, el compromiso de Spectrum con un manejo responsable del agua es parte fundamental de su visión de sostenibilidad. El desarrollador inmobiliario líder de centros comerciales, vivienda y oficinas está consciente que el futuro de la economía debe ser verde y que su éxito está 
                íntimamente ligado al cuidado del planeta. Por eso, cuenta con iniciativas de gran alcance en el uso eficiente y reducción de consumo del agua que han logrado un impacto positivo en el país.`,
                img_1: './assets/img/articulos/art_27/diaagua1.jpg',
                text_2: `Hechos, no palabras

                Para cuidar el agua, Spectrum analiza todo el recorrido del agua para optimizar su uso por medio de tres tipos de acciones: ahorro de agua potable y agua gris, estrategias de re-uso de agua y tratamiento de aguas residuales. Estas iniciativas son transversales, es decir, las implementa tanto en sus centros comerciales como en sus proyectos de vivienda y oficina. Con la instalación y uso de artefactos ahorradores en los baños, riego por goteo, instalación de plantas nativas de bajo mantenimiento y el mantenimiento preventivo de instalaciones hidráulicas, como bombas, pozos y cisternas para optimizar su funcionamiento y controlar fugas, el desarrollador inmobiliario ha logrado ahorrar en un 28% su consumo de agua potable.
                
                “Lo bueno de esta iniciativa es que con ella no solo cumplimos con nuestro compromiso con una operación de excelencia que deje huella positiva en nuestro ambiente, sino también implicamos a las familias que viven en nuestros proyectos de vivienda en el cuidado del agua, pues únicamente con los artefactos ahorradores hemos logrado que estos hogares ahorren un 33% en su consumo de agua”, señala Belem Salomon, gerente de desarrollo responsable de Spectrum.
                
                Según explica Salomon, esta es la principal diferencia del enfoque del desarrollador, hacer partícipe a su comunidad en sus acciones de desarrollo responsable. “Todos estamos conscientes de la importancia de cuidar nuestro planeta y todos queremos ser parte de estos esfuerzos. Por eso, Spectrum busca consistentemente soluciones innovadoras que abran la oportunidad a nuestros clientes de sumarse a mejorar su entorno, agrega la gerente de desarrollo responsable.`,
                img_2: './assets/img/articulos/art_27/diaagua4.png',
                text_3:`Las estrategias para el re-uso de agua que Spectrum impulsa incluyen la recirculación de agua en muros verdes, captación de condensación de agua de aire acondicionado, recolección de agua gris para riego y corrección de descargas de aguas residuales y pluviales.
                
                Desde que Spectrum implementa estas iniciativas de uso eficiente de este vital líquido ha ahorrado 980,000 metros cúbicos de agua potable. “Esto es equivalente a 392 piscinas olímpicas”, apunta Belem Salomon, quien destaca que estos resultados son los que hacen la diferencia en la vida de las personas, sobre todo si se toma en cuenta que, de acuerdo con la Organización para la Cooperación y el Desarrollo Económicos (OCDE), la demanda de agua se ha disparado globalmente hasta un 55% entre los años 2000 y 2050, por lo que es cada vez más importante un uso eficiente.

                En su tercera vía de acción relacionado con este vital líquido, Spectrum realiza actividades de tratamiento de aguas residuales de forma permanente, como aplicación de enzimas y desnatado en trampas de grasa primarias y secundarias, limpieza y extracción de sedimentos en pozos de bombeo y tratamientos primarios, y extracción y disposición de lodos de forma certificada, entre otras acciones.

                “En Spectrum, hemos demostrado que cuidar nuestros recursos naturales es un buen negocio, que el éxito va de la mano de buenas prácticas ambientales. Cuando la sostenibilidad se convierte en acciones concretas que aporta beneficios tangibles a las comunidades en donde operamos, inspira a otros a sumarse y soñar en construir un mundo mejor. Y esa es precisamente nuestra visión, desarrollar los sueños y colaborar para que otros puedan alcanzarlos”, puntualizó la gerente de desarrollo responsable de Spectrum.`,
                img_3:'./assets/img/articulos/art_27/diaagua3.jpg',
                number: 26
            },
            {
                img: './assets/img/articulos/art_29/thumbnail_editoriales_summerfashion.png',
                title: 'SUMMER FASHION',
                caption:'3 prendas para este verano.',
                text_1: `1. Track Suit.
                
                Las pasarelas para la Primavera | Verano 2021 estuvieron repletas de trajes, siluetas de los 80’s y el athleisure. El track suit ahora es un esencial de nuestro closet. Tom Ford y Rodarte le agregaron glamour a esta tendencia utilizando satin para los pants y pijamas. El color mas popular fue el azul en todas sus gamas, inspirado en los cielos, mares dándole un toque de relajación.`,
                img_1: './assets/img/articulos/art_29/balenciaga_tracksuit.png',
                text_2: `2. Flip-Flops
                
                Un nuevo statement en zapatos son los flip-flops. Tom Ford le agrega un toque clásico y lujoso a las clásicas flip-flops. Jacquemus nos presenta un modelo con plataforma y Stella McCartney ese estilo mas deportivo. Los tonos neutros tales como el negro, café y blanco complementan cualquier look para estas vacaciones.`,
                img_2: './assets/img/articulos/art_29/jacquemus_flipflop.png',
                text_3:`3. Playeras

                Las playeras con diseño se han convertido en un clásico del streetwear por muchas temporadas y para este verano 2021 no seria la excepción. Han tomado una nueva importancia con todo lo acontecido durante el 2020. Louis Vuitton y Balenciaga apostaron por moda sostenible, enfocado en playeras con diseño y diversas frases. En otras marcas pudimos observar la mezcla de los logos clásicos, el feel de streetwear moderno con gráficos neón, inspirado en los 80’s.
                
                La manera en como nos vestimos puede tener un gran impacto en como nos sentimos y también refleja nuestro estado anímico, así que para estas colecciones los diseñadores re examinaron el propósito de cada pieza y lo que necesitamos en la moda.
                
                De las tendencias que se pudieron observar fueron detalles fabulosos y coloridos, el cual busca ese placer de vestirnos y otra que agrega piezas clásicas a los outfits que utilizamos el 2020.

                Las rayas de arco iris fueron el símbolo no oficial para el 2020, como referencia de esperanza y optimismo, así que para este verano 2021 el tie-dye y las rayas siguen plasmándose en estampados en distintas piezas de Versace, Gucci y Rossie Assoulin.`,
                img_3:'./assets/img/articulos/art_29/stella_mccartney_flipflops.png',
                number: 27
            },
            {
                img: './assets/img/articulos/art_30/Artículo-abril-spectrum_Thumbnail.png',
                title: 'Spectrum y su impacto en el medio ambiente',
                caption:'A través de los años, en Spectrum hemos trabajado y demostrado nuestro compromiso con el medio ambiente.',
                text_1: `A través de los años, en Spectrum hemos trabajado y demostrado nuestro compromiso con el medio ambiente. En este mes tan importante, en el que celebramos el Día de la Tierra, creemos que el futuro de la economía debe ser verde y que su éxito está íntimamente ligado al cuidado del planeta; mediante acciones que tengan un impacto positivo en la protección del medio ambiente y en la calidad de vida de las personas.


                Con 28 proyectos de vivienda desarrollados, 7 centros comerciales en operación, 1 centro de negocios y 1 centro cultural, somos el desarrollador con más metros cuadrados certificados LEED en Centro América, gracias a nuestro compromiso con la sostenibilidad. Esto significa que contamos con 5 proyectos certificados LEED, los cuales se han logrado a las buenas prácticas como priorizar la ventilación e iluminación natural desde el diseño, optimizar el uso de los recursos 
                como agua y energía por medio de eficiencias en equipos y artefactos ahorradores y el uso de materiales regionales, entre otras.`,
                img_1: './assets/img/articulos/art_30/Artículo-abril-spectrum_foto 05.png',
                text_2: `Participamos activamente en proyectos de optimización de agua con tres tipos de ahorro que se implementan en centros comerciales y proyectos de vivienda y oficina. Con la instalación y uso de artefactos ahorradores en los baños, riego por goteo, instalación de plantas nativas de bajo mantenimiento y el mantenimiento preventivo de instalaciones hidráulicas, como bombas, pozos y cisternas para optimizar su funcionamiento y controlar fugas. Logrando ahorrar hasta un 28% de consumo de agua potable en nuestros proyectos.

                Además, participamos en proyectos de reforestación de importantes áreas metropolitanas, con la siembra de 29,745 árboles, los cuales recargan alrededor de 6,000 piscinas olímpicas al año.`,
                img_2: './assets/img/articulos/art_30/Artículo-abril-spectrum_foto 03.png',
                text_3:`Nuestro propósito es transformar positivamente las ciudades, y la calidad de vida de las personas, mediante proyectos rentables y sostenibles a largo plazo.`,
                img_3:'./assets/img/articulos/art_30/Artículo-abril-spectrum_foto 02.png',
                number: 28
            },
            {
                img: './assets/img/articulos/art_30/4.png',
                title: 'Papá Portales',
                caption:'A través de los años, en Spectrum hemos trabajado y demostrado nuestro compromiso con el medio ambiente.',
                text_1: `El Día del Padre se acerca y puedes encontrar el detalle o regalo para sorprender a papá en Portales. Los regalos varían con el tiempo y los clásicos como corbatas, lociones y relojes siguen siendo la opción, aunque la modernidad ha creado nuevas opciones que le encantaran.

                En Portales puedes encontrar muchas opciones para crear diferentes looks para ese papá especial y diferente, así también como esos accesorios tecnológicos que tanto le encantan.
                
                Para esos papas modernos que les gusta un estilo arriesgado, las opciones como calcetines con un diseño moderno y colorido hará que se sorprenda y salga de lo tradicional, en Emporium encontraras una amplia variedad de diseños y colores.
                
                El papa deportista le encantara un gadget fitness como un reloj que monitoree su actividad física, unos audífonos inalámbricos, hasta una pantaloneta o tennis para que disfrute sus actividades con el mejor estilo siempre.
                
                El papa ejecutivo con su rutina que no se detiene, puedes escoger un accesorio que lo acompañe en su día a día y complemente su estilo, ese detalle personalizado como las mancuernillas, serán un regalo grandioso.
                
                Esta es tu oportunidad para elegir y regalarle algo diferente a papa, ese ejemplo a seguir, y agradecerle esa compañía en los momentos mas importantes de tu vida.`,
                img_1: '',
                text_2: ``,
                img_2: '',
                text_3:``,
                img_3:'',
                number: 29
            },
            {
                img: './assets/img/articulos/art_31/PS_Articulo_Thumbnail.png',
                title: '¡En Portales, las ideas se hacen realidad!',
                caption:'¡9 años cumpliendo sueños! Desde el 2013, el programa de Mujeres Emprendedoras de Portales, apoya a todas las mujeres con un sueño.',
                text_1: `¡9 años cumpliendo sueños! Desde el 2013, el programa de Mujeres Emprendedoras de Portales, apoya a todas las mujeres con un sueño, ayudándolas a hacerlo realidad. Con 6 ediciones, hemos logrado capacitar a más de 580 mujeres; empoderándolas y llevando su idea de negocio a otro nivel, 
                brindándoles las herramientas necesarias para impulsar su emprendimiento. Este año, en nuestra séptima edición, con alianza de Bac Credomatic y Wonder Woman, nos unimos nuevamente con la misión de apoyar el emprendimiento de la mujer en Guatemala.
                
                Este magnífico programa, cuenta con 10 sesiones de formación, cada una posee herramientas indispensables para que nuestras participantes tengan el conocimiento necesario para fortalecer la capacidad de su negocio, desde talleres de productos y servicios hasta herramientas financieras para comercializar su producto.

                ¡Mujeres fuertes y más seguras!
                Como parte de nuestro compromiso, estimulamos el sentido de pertenencia, autoestima y autonomía económica de cada una de nuestras emprendedoras, dándoles el apoyo necesario para reinventarse, asumiendo el rol activo en el desarrollo económico de su familia.`,
                img_1: './assets/img/articulos/art_31/PS_Articulo_02.png',
                text_2: `Cada una de ellas, tendrá la oportunidad de ser una semifinalista en un taller de presentación y estrategia, pasando a finalistas en un foro con mujeres líderes con la fabulosa oportunidad de participar en un mercadito dentro de Portales. La ganadora, podrá poner en práctica lo aprendido, en su premio espectacular, de un kiosco durante la época más alta del año en el Centro Comercial.

                Estamos listos para empoderar y enseñar. ¡Déjanos hacer tu idea realidad!`,
                img_2: './assets/img/articulos/art_31/PS_Articulo_03.png',
                text_3:``,
                img_3:'',
                number: 30
            },
            {
                img: './assets/img/articulos/art_32/img-Thumbnail.png',
                title: '¡En Portales, las ideas se hacen realidad!',
                caption:'¿Ideas para tu feriado? Te compartimos algunas ideas para disfrutar tu descanso al máximo y probar todos los sabores de Guatemala',
                text_1: `¿Ideas para tu feriado? Te compartimos algunas ideas para disfrutar tu descanso al máximo y probar todos los sabores de Guatemala en un solo lugar, Portales.

                ¡Inicia el día lleno de energía! ¿Eres de los que necesita un buen café para despertar? Tu opción favorita la podrás encontrar en @CafeBaristaLovers, @McCafé e @&Café, desde un espresso guatemalteco hasta un latte saborizado ¡te encantarán!
                
                ¡Desayuno en familia! Si entre tus planes está disfrutar de un delicioso desayuno en #familia, @SanMartín será tu mejor opción, desde un tradicional pan con chile hasta un riquísimo rellenito de postre. Encuentra deliciosos platillos para compartir con el delicioso y auténtico sabor chapín.`,
                img_1: './assets/img/articulos/art_32/img-1.png',
                text_2: `¿Hora de almuerzo? Si después de tus compras, el antojo de algo delicioso viene a ti, en @laestancia podrás encontrar una tradicional y deliciosa opción, parrilladas chapinas para compartir o solo para ti. ¿Cuál será tu favorita?

                ¡El postre no puede faltar! Las donas de @KrispyKreme y @AmericanDunuts te dejarán sin palabras, dona rellenas y de temporada te estarán esperando. Si lo tuyo es algo refrescante, la Banana Patria de @Sarita te encantará.`,
                img_2: './assets/img/articulos/art_32/img-4.png',
                text_3:`¡No puedes irte sin pensar en decorar tu hogar! Ven a @Cemaco y apoya al artesano guatemalteco, encontrarás gran variedad de artículos, hechos para ti.

                Y para terminar el día con broche de oro, ¡@Cinépolis! Ven y disfruta con toda tu familia de tu función favorita, con todas las medidas de seguridad.
                
                ¿Aceptas? ¡Ven y celebra a Guatemala en Portales!`,
                img_3:'./assets/img/articulos/art_32/img-3.png',
                number: 31
            },

        ]
    },
    rooftop: {
        categories: [
            {
                title: 'Concepto de postres',
                img: './assets/img/home/home-page-benefits-placeholder.png',
                category_items: [
                    {
                        name: 'Parisino',
                        img: './assets/img/rooftop/placeholder.png'
                    }
                ]
            },
            {
                title: 'Salón y spa',
                img: './assets/img/home/home-page-benefits-placeholder.png',
                category_items: [
                    {
                        name: 'Rebecana',
                        img: './assets/img/rooftop/placeholder.png'
                    }
                ]
            },
            {
                title: 'Academias',
                img: './assets/img/home/home-page-benefits-placeholder.png',
                category_items: [
                    {
                        name: 'MUD',
                        img: './assets/img/rooftop/placeholder.png'
                    },
                    {
                        name: 'Micheo Boxing',
                        img: './assets/img/rooftop/placeholder.png'
                    },
                    {
                        name: 'Femme Fit',
                        img: './assets/img/rooftop/placeholder.png'
                    },
                    {
                        name: 'Woodstock',
                        img: './assets/img/rooftop/placeholder.png'
                    },
                ]
            },
            {
                title: 'Restaurante de tenedor',
                img: './assets/img/home/home-page-benefits-placeholder.png',
                category_items: [
                    {
                        name: 'Nikkei',
                        img: './assets/img/rooftop/placeholder.png'
                    },
                    {
                        name: 'Kloster',
                        img: './assets/img/rooftop/placeholder.png'
                    },
                    {
                        name: 'Applebees',
                        img: './assets/img/rooftop/placeholder.png'
                    },
                    {
                        name: 'Skillets',
                        img: './assets/img/rooftop/placeholder.png'
                    },
                    {
                        name: 'La Monumental',
                        img: './assets/img/rooftop/placeholder.png'
                    },
                    {
                        name: 'Don Emiliano',
                        img: './assets/img/rooftop/placeholder.png'
                    },
                    {
                        name: 'Biba',
                        img: './assets/img/rooftop/placeholder.png'
                    },

                ]
            },

        ]
    },
    services: {
        fares: [
            {
                title: 'Vehiculos',
                img: './assets/img/icons/car.png',
                detail: 'Q15 de 1 a 4 horas. Más de 4 horas Q50 De 11 am en adelante.'
            },
            {
                title: 'Motos y Taxis',
                img: './assets/img/icons/bike.png',
                detail: 'Q15 de 1 a 4 horas. Más de 4 horas Q50 De 11 am en adelante.'
            },
            {
                title: 'Bicicletas',
                img: './assets/img/icons/bicycle.png',
                detail: 'Gratis el parqueo. Hay espacio en sótano 1 Sur y en fachada (6 espacios)'
            }
        ]
    }
}


export {
    jsonData
}