const animateCSS = (element, animation, duration, prefix = 'animate__') =>
  // We create a Promise and return it
  new Promise((resolve, reject) => {
    const animationName = `${prefix}${animation}`;
    
    element.style.setProperty('--animate-duration', duration);
    element.classList.add(`${prefix}animated`, animationName);

    // When the animation ends, we clean the classes and resolve the Promise
    function handleAnimationEnd(event) {
      event.stopPropagation();
      element.classList.remove(`${prefix}animated`, animationName);
      resolve('Animation ended');
    }

    element.addEventListener('animationend', handleAnimationEnd, {once: true});
  });

function loadingIcon(element) {
    element.classList.add('disabled');
    element.innerHTML = '<i class="now-ui-icons loader_gear spin"></i>';
}

function stopLoadingIcon(element, htmlContent) {
    element.classList.remove('disabled');
    element.innerHTML = htmlContent;
}



