const creditCardValidation = function (card) {
    const firstNumber = card.slice(0, 1);
    switch (firstNumber) {
        case '3':
            return isAmericanExpress();
        case '4':
            return isVisa();
        case '5':
            return isMastercard();
        default:
            return false;
    }

}

function isVisa() {
    const regEx = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;
    if (inputtxt.value.match(regEx)) {
        return true;
    }
    else {
        return false;
    }
}

function isAmericanExpress() {
    const regEx = /^(?:3[47][0-9]{13})$/;
    if (inputtxt.value.match(regEx)) {
        return true;
    }
    else {

        return false;
    }
}

function isMastercard() {
    const regEx = /^(?:5[1-5][0-9]{14})$/;
    if (inputtxt.value.match(regEx)) {
        return true;
    }
    else {
        return false;
    }
}

export { creditCardValidation }