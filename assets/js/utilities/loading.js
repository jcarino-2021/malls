
Noty.overrideDefaults({
  layout: 'topRight',
  theme: 'nest',
  closeWith: ['click', 'button']
});

/**
 * 
 * @param {*} type alert, success, error, warning, info -
 * @param {*} layout top, topLeft, topCenter, topRight, center, centerLeft, centerRight, bottom, bottomLeft, bottomCenter, bottomRight
 * @param {*} text string
 * @param {*} timeout miliseconds
 */
function showLoader(refCtx) {
  return Vue.$loading.show({
    // Pass props by their camelCased names
    container: refCtx.$refs.loadingContainer,
    canCancel: true, // default false
    onCancel: () => { console.log('cancel'); },
    color: '#600716',
    loader: 'spinner',
    width: 64,
    height: 64,
    backgroundColor: '#000000',
    opacity: 1,
    zIndex: 99999,
  }, {
    // Pass slots by their names
    //default: this.$createElement('your-custom-loader-component-name'),
  });
  
  
}

function hideLoader(loader) {
  loader.hide();
}

const loadingUtil = { showLoader, hideLoader }

export { loadingUtil }



