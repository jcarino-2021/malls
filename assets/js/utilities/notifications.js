
 Noty.overrideDefaults({
    layout: 'topRight',
    theme: 'nest',
    closeWith: ['click', 'button']
});

/**
 * 
 * @param {*} type alert, success, error, warning, info -
 * @param {*} layout top, topLeft, topCenter, topRight, center, centerLeft, centerRight, bottom, bottomLeft, bottomCenter, bottomRight
 * @param {*} text string
 * @param {*} timeout miliseconds
 */
function showNotification(type, layout, text, timeout) {
    new Noty({
        type,
        layout,
        text,
        timeout
    }).show();
}


function showDialog() {
    var n = new Noty({
        text: 'Do you want to continue? <input id="example" type="text">',
        buttons: [
          Noty.button('YES', 'btn btn-success', function () {
              console.log('button 1 clicked');
          }, {id: 'button1', 'data-status': 'ok'}),
      
          Noty.button('NO', 'btn btn-error', function () {
              console.log('button 2 clicked');
              n.close();
          })
        ]
      });
      n.show();
}

function showConfirmationSwal(title, text, confirmButtonText, cancelButtonText) {
    return new Promise(resolve => {
        Swal.fire({
            title,
            text,
            cancelButtonText,
            confirmButtonText,
            showCancelButton: true,
            reverseButtons: true
            
        }).then(result => {
            console.log(result);
            if(result.value){
                resolve(true);
            } else {
                resolve(false);
            }
        });
    });
    
}

const notifications = {showNotification, showDialog, showConfirmationSwal}

export {notifications}



