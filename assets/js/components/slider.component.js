import { configurations } from '../configurations.js';
const slider = Vue.component(
    'slider-component', {
    props: ['slides'],
    template: `
<!-- CARRUSEL -->
<!-- Top content -->
<div class="top-content mb-4" v-if="slides">
    <!-- Carousel -->
    <div id="carousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li 
            v-for="(slide, index) in slides"
            data-target="#carousel" :data-slide-to="index" v-bind:class="{ active: index === 0 }"></li>
        </ol>
 
        <div class="carousel-inner">
            <div class="carousel-item"  v-bind:class="{ active: index === 0, no_fullscreen_slider: !slide.is_fullscreen }" v-for="(slide, index) in slides">
            <img v-if="!slide.is_video" :src="slide['image'].url" class="d-block w-100" alt="slide-img-1">
            <div v-if="slide.is_video" class="video-container">
            <div class="video-overlay" :style="{backgroundColor: slide['video_overlay_color']}"></div>
                <video autoplay muted loop>
                    <source :src="slide.video.url" type="video/mp4" />
                </video></div>
            
                <div class="carousel-caption">
                    <h1 class="special-font">{{slide.title}}</h1>
                    <div v-if="slide.subtitle" class="carousel-caption-description">
                        <p>{{slide.subtitle}}</p>
                    </div>
                    <a v-if="slide.call_to_action_url" :href="slide.call_to_action_url" class="btn btn-outline-secondary">
                        {{slide.call_to_action_text}}
                    </a>
                </div>
            </div>
        </div>
 
        <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- End carousel -->
</div>       
        `
});


export { slider };