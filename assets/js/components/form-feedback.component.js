
const formFeedbackComponent = Vue.component(
    'form-feedback-component', {
    props: ['isValid', 'message'],
    template: `<div v-bind:class="{'valid-feedback': isValid, 'invalid-feedback': !isValid}">
                    {{message}}
                </div>`
});
export {formFeedbackComponent}