var gulp = require('gulp');
var path = require('path');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var open = require('gulp-open');
var fs = require('fs');
var GulpSSH = require('gulp-ssh')

var Paths = {
  HERE: './',
  DIST: 'dist/',
  CSS: './assets/css/',
  SCSS_TOOLKIT_SOURCES: './assets/scss/now-ui-kit.scss',
  SCSS: './assets/scss/**/**'
};

gulp.task('compile-scss', function () {
  return gulp.src(Paths.SCSS_TOOLKIT_SOURCES)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write(Paths.HERE))
    .pipe(gulp.dest(Paths.CSS));
});




const uploadTo = "/var/www/html/oakland/megasale"

gulp.task('deploy', function () {
  var config = {
    host: 'ec2-3-22-100-149.us-east-2.compute.amazonaws.com',
    port: 22,
    username: 'ec2-user',
    privateKey: fs.readFileSync('/Users/danielcastillo/Downloads/landing-malls.ppk')
  }

  var gulpSSH = new GulpSSH({
    ignoreErrors: false,
    sshConfig: config
  })
  const globs = [
    /* Full project */
  /*    'assets/css/**',
    'assets/fonts/**',
    'assets/img/**',
    'assets/js/**',
    'assets/videos/**',
    'includes/**',
    '*.php', */


    //------------------
    /* Individual files  */
    //"test.php"
    "includes/constants.php"
  ];

  // using base = '.' will transfer everything to /public_html correctly
  // turn off buffering in gulp.src for best performance

  return gulp.src(globs, { base: '.', buffer: false })
    .pipe(gulpSSH.dest(uploadTo));

});