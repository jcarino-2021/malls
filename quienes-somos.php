<?php include('./includes/constants.php'); ?>
<!-- Constantes de la aplicación -->
<?php include('./includes/header.php'); ?>

<div id="app" v-cloak>
    <!-- Slider -->
    <div class="container-fluid d-flex align-items-center justify-content-center" style="background-image: linear-gradient(rgba(0,0,0,0.2),rgba(0,0,0,0.8)), url('./assets/img/quienes-somos/header.jpg'); height: 550px; background-size: cover; background-position: center;">
        <h2 class="text-white special-font">Quienes somos</h2>
    </div>
    <div class="container my-10">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Portales, hecho para mi</h2>
                <p class="text-muted font-light">Ubicado sobre la Carretera al Atlántico de la Ciudad de Guatemala, Portales es el corazón de la zona 17 y el lugar ideal para vivir inolvidables experiencias con familia y amigos. </p>
            </div>
        </div>
    </div>
    <div class="container-fluid my-5 left-image-right-content">
        <div class="row my-4">
            <div class="col-md-6 px-0">
                <img src="./assets/img/quienes-somos/somos.png" alt="">
            </div>
            <div class="col-md-6 d-flex flex-column align-items-center justify-content-center mt-sm-5">
                <div class="content-side">

                    <h2 class="font-light">Somos</h2>
                    <p class="text-muted font-light">Con 83 mil m2 de construcción, Portales cuenta con una mezcla comercial hecha especialmente para la zona. Cuenta con más de 200 opciones de compra entre  moda, tecnología, conveniencia, artículos para el hogar, regalos y mucho más, para que los visitantes encuentren todo lo necesario. </p>
                </div>

            </div>
        </div>
    </div>
    <div class="container-fluid my-10 right-image-left-content ">
        <div class="row my-4">
            <div class="col-md-6 d-flex flex-column align-items-center justify-content-center  order-2 order-sm-2 order-md-1 order-sm-2 mt-sm-5">
                <div class="content-side">

                    <h2 class="font-light">Servicios</h2>
                    <p class="text-muted font-light">Además, tiene la plaza financiera más completa de la ciudad, amplio estacionamiento, facilidades para personas embarazadas, con hijos y con capacidades especiales, una serie de amenidades para niños e internet inalámbrico de banda ancha gratis en todo el centro comercial.</p>
                </div>
            </div>
            <div class="col-md-6 px-0 order-1 order-sm-1  order-md-2">
                <img src="./assets/img/quienes-somos/servicios.png" alt="">
            </div>
        </div>
    </div>
    <div class="container-fluid px-5">
        <h2 class="text-center">Visitar otros malls</h2>
        <div class="row about-us-malls-images">
            <div>
                <a href="https://www.oaklandmall.com.gt/" target="blank">
                    <img src="./assets/img/quienes-somos/oakland-mall.png" alt=""></a>

            </div>
            <div>
                <a href="https://miraflores.com.gt/" target="blank">
                    <img src="./assets/img/quienes-somos/miraflores-mall.png" alt="">
                </a>

            </div>
            <div>
                <a href="https://naranjomall.com.gt/" target="blank">
                    <img src="./assets/img/quienes-somos/naranjo-mall.png" alt=""></a>

            </div>
            <div>
                <a href="https://portales.com.gt/" target="blank">
                    <img src="./assets/img/quienes-somos/portales-mall.png" alt="">
                </a>

            </div>
            <div>
                <a href="https://www.museomiraflores.org.gt/" target="blank">
                    <img src="./assets/img/quienes-somos/museo-miraflores.png" alt="">
                </a>

            </div>
        </div>
    </div>
</div>


<?php include('./includes/footer-includes.php'); ?>
<!-- Load js used in this page -->
<script type="module" src="./assets/js/scripts/about-us.js"></script>
<?php include('./includes/footer.php'); ?>