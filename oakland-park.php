<?php include('./includes/constants.php'); ?>
<!-- Constantes de la aplicación -->
<?php include('./includes/header.php'); ?>

<div id="app" v-cloak>
    <!-- Slider -->
    <div class="container-fluid d-flex align-items-center justify-content-center" style="background-image: linear-gradient(rgba(0,0,0,0.2),rgba(0,0,0,0.8)), url('./assets/img/pinata/header.jpeg'); height: 550px; background-size: cover; background-position: center;">
        <h2 class="text-white font-light">Piñatas</h2>
    </div>
    <div class="container my-5">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Oakland Park</h2>

            </div>
        </div>
    </div>
    <div class="container-fluid my-5 left-image-right-content">
        <div class="row my-4">
            <div class="col-md-6 px-0">
                <img src="./assets/img/pinata/first-section.png" alt="">
            </div>
            <div class="col-md-6 d-flex flex-column align-items-center justify-content-center mt-sm-5">
                <div class="content-side">
                <h3 class="font-light">Exceptional Land</h3>
                    <h2 class="font-light">La diversión a otro nivel. Amenidades ideales para celebrar momentos únicos.</h2>
                    <p class="text-muted font-light">Área de juegos en Oakland Mall. La capacidad máxima es de 70 personas por evento.</p>
                    <p class="text-muted font-light" style="font-size: 0.8rem;">Debido a las disposiciones gubernamentales el máximo es de 60 personas.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container p-0 mt-5">
        <div class="row">
            <div class="col-md-12 p-0">
                <img src="./assets/img/pinata/book-now.png" alt="Reserva ya">
                <div class="row" style="position:absolute; bottom: 5px; right: 15px; width: 100%; padding:15px 25px 0 25px;">
                    <div class="col-md-6 text-white">
                        <p class="font-light">Conoce todo lo que incluye el paquete de piñatas</p>
                        <p class="font-light">Ver más</p>
                    </div>
                    <div class="col-md-6 text-right">
                        <button class="btn btn-info btn-round" style="min-width: 150px;">Reservar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<?php include('./includes/footer-includes.php'); ?>
<!-- Load js used in this page -->
<script type="module" src="./assets/js/scripts/pinata.js"></script>
<?php include('./includes/footer.php'); ?>