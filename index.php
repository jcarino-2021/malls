<?php 
//header('Location: megasale.php');
?>

<?php include('./includes/constants.php'); ?>
<!-- Constantes de la aplicación -->
<?php include('./includes/header.php'); ?>

<div id="app" v-cloak>
    <!-- Slider -->
    <slider-component :slides="homeData.slides"></slider-component>
    <div class="container my-5">
        <div class="row">
            <div class="col-md-6 d-flex flex-column text-center align-items-center justify-content-center text-uppercase">
                <h2 class="mb-2">Directorio</h2>
                <a class="btn btn-info btn-round" href="directorio.php" style="min-width: 150px;">Ver todo</a>
            </div>
            <div class="col-md-6">
                <div class="row justify-content-center">
                    <div class="col-md-6 text-center directory-home-item">
                        <img src="./assets/img/icons/banks.svg" alt="">
                        <p>Bancos</p>
                    </div>
                    <div class="col-md-6 text-center directory-home-item">
                    <img src="./assets/img/icons/clothes.svg" alt="">
                        <p>Ropa</p>
                    </div>
                    <div class="col-md-6 text-center directory-home-item">
                    <img src="./assets/img/icons/plate.svg" alt="">
                        <p>Comida</p>
                    </div>
                    <div class="col-md-6 text-center directory-home-item">
                    <img src="./assets/img/icons/tech.svg" alt="">
                        <p>Tecnología</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="container-fluid my-5 left-image-right-content">
        <div class="row my-4">
            <div class="col-md-6 px-0">
                <img src="./assets/img/home/stylish-woman-listening-music-on-her-airpods-in-ci-QY-1.png" alt="">
            </div>
            <div class="col-md-6 d-flex flex-column align-items-center justify-content-center  mt-sm-5">
                <div class="content-side">
                    <h3 class="font-light">Beneficio</h3>
                    <h2>Gana unos Airpods con Spectrum</h2>
                    <p class="text-muted font-light">Ven a Roof Top de Oakland Mall este viernes y sábado 4 y 5 de Mayo. Entrada Q100.00. Música en vivo y boquitas.</p>
                </div>

            </div>
        </div>
    </div>
    <div class="container-fluid my-10 right-image-left-content ">
        <div class="row my-4">
            <div class="col-md-6 d-flex flex-column align-items-center justify-content-center order-2 order-sm-2  order-md-1 mt-sm-5">
                <div class="content-side">
                    <h3 class="font-light">Eventos</h3>
                    <h2>Disfruta de una noche de música innolvidable</h2>
                    <p class="text-muted font-light">Ven a Roof Top de Oakland Mall este viernes y sábado 4 y 5 de Mayo. Entrada Q100.00. Música en vivo y boquitas.</p>
                </div>
            </div>
            <div class="col-md-6 px-0 order-1 order-sm-1  order-md-2">
                <img src="./assets/img/home/Banner 1.png" alt="">
            </div>
        </div>
    </div> -->
    <div class="container-fluid my-5 left-image-right-content">
        <div class="row my-4">
            <div class="col-md-6 px-0">
                <img src="./assets/img/home/stylish-woman-listening-music-on-her-airpods-in-ci-QY-1.png" alt="">
            </div>
            <div class="col-md-6 d-flex flex-column align-items-center justify-content-center  mt-sm-5">
                <div class="content-side">
                    <h3 class="font-light">Beneficio</h3>
                    <h2 class="font-weight-light">Gana unos Airpods</h2>
                    <p class="text-muted font-light">¡Carga tu factura arriba de Q500 y podrás ser uno de los ganadores mensuales!</p>
                </div>

            </div>
        </div>
    </div>
    <div class="container-fluid my-10 right-image-left-content " v-if="upcomingEvent">
        <div class="row my-4">
            <div class="col-md-6 d-flex flex-column align-items-center justify-content-center order-2 order-sm-2  order-md-1 mt-sm-5">
                <div class="content-side">
                    <h3 class="font-light">Eventos</h3>
                    <h2>{{upcomingEvent.name}}</h2>
                    <p class="text-muted font-light">{{upcomingEvent.description}}</p>
                </div>
            </div>
            <div class="col-md-6 px-0 order-1 order-sm-1  order-md-2">
                <img :src="upcomingEvent.image" alt="">
            </div>
        </div>
    </div>
    <div class="container my-3">
        <div class="row">
            <div class="col-md-12">
                <img src="./assets/img/home/el-circo.png" alt="">
            </div>
        </div>
    </div>
    <!-- <div class="container my-5">
        <div class="row">
            <div class="col-md-12">
                <h1 class="special-font-title text-center">Blog</h1>
                
            </div>
            <div class="col-md-4 text-center" v-for="blogPost in homeData.blog_posts">
                <img :src="blogPost.img" alt="">
                <div class="mt-4 d-flex align-items-center justify-content-center" style="height: 55px; line-height: 18px !important;">
                    <p class="px-4" style="font-size: 1.3rem; width: 80%;">
                        {{blogPost.title}}
                    </p>
                </div>
                <div stlye="height: 75px;">
                    <p class="text-muted font-light px-3" style="font-size: 0.7rem; height: 75px;">{{blogPost.caption}}</p>
                </div>
            </div>
            <div class="col-md-12 text-center">
                <a class="btn btn-info btn-round" href="blog.php" style="min-width: 150px;">Ver todo</a>
            </div>
        </div>
    </div>
 -->
</div>


<?php include('./includes/footer-includes.php'); ?>
<!-- Load js used in this page -->
<script type="module" src="./assets/js/scripts/home.js"></script>
<?php include('./includes/footer.php'); ?>