<?php include('./includes/constants.php'); ?>
<!-- Constantes de la aplicación -->
<?php include('./includes/header.php'); ?>

<div id="app" v-cloak>
    <!-- Slider -->
    <div class="container-fluid d-flex align-items-center justify-content-center" style="background-image: linear-gradient(rgba(0,0,0,0.2),rgba(0,0,0,0.8)), url('./assets/img/beneficios/header.jpg'); height: 550px; background-size: cover; background-position: center;">
        <h2 class="text-white special-font">Beneficios</h2>
    </div>
    <div class="container my-5">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="font-weight-bold ">Beneficios actuales</h2>

            </div>
        </div>
    </div>
    <!-- <div v-for="(specialBenefit, index) in specialBenefits">

        <div v-if="(index + 1)%2 > 0" class="container-fluid my-5 left-image-right-content">
            <div class="row my-4">
                <div class="col-md-6 px-0">
                    <img :src="specialBenefit.store.avatar" :alt="specialBenefit.name">
                </div>
                <div class="col-md-6 d-flex flex-column align-items-center justify-content-center mt-sm-5">
                    <div class="content-side">

                        <h2 class="font-light">{{specialBenefit.name}}</h2>
                        <p class="text-muted font-light">{{specialBenefit.description}}</p>
                    </div>

                </div>
            </div>
        </div>
        <div v-if="(index + 1)%2 === 0" class="container-fluid my-10 right-image-left-content ">
            <div class="row my-4">
                <div class="col-md-6 d-flex flex-column align-items-center justify-content-center  order-2 order-sm-2 order-md-1 mt-sm-5">
                    <div class="content-side">
                        <h2 class="font-light">{{specialBenefit.name}}</h2>
                        <p class="text-muted font-light">{{specialBenefit.description}}</p>
                    </div>
                </div>
                <div class="col-md-6 px-0  order-1 order-sm-1  order-md-2">
                    <img :src="specialBenefit.store.avatar" :alt="specialBenefit.name">
                </div>
            </div>
        </div>
    </div> -->
    <div class="container-fluid my-5 left-image-right-content">
        <div class="row my-4">
            <div class="col-md-6 px-0">
                <img src="./assets/img/home/stylish-woman-listening-music-on-her-airpods-in-ci-QY-1.png" alt="">
            </div>
            <div class="col-md-6 d-flex flex-column align-items-center justify-content-center  mt-sm-5">
                <div class="content-side">
                    <h3 class="font-light">Beneficio</h3>
                    <h2 class="special-font font-weight-light">Gana unos Airpods</h2>
                    <p class="text-muted font-light">¡Carga tu factura arriba de Q500 y podrás ser uno de los ganadores mensuales!</p>
                </div>

            </div>
        </div>
    </div>
    <hr style="border: 0.7px solid #dadada; width: 60%" class="mx-autp">
    <div class="container my-5" v-if="coupons.length">
        <div class="row">
            <div class="col-md-12">
                <h2 class="font-weight-bold text-center">Beneficios</h2>

            </div>
            <div class="col-md-4 text-center" v-for="coupon in coupons">
                <img :src="coupon.image" :alt="coupon.name" style="min-height: 210px;" v-on:click="selectCouponAndOpenModal(coupon)">
                <div class="px-2 d-flex align-items-center justify-content-center" style="height: 120px;overflow:hidden;">
                    <p style=" line-height: 18px !important ">
                        {{coupon.name}}
                    </p>
                </div>

                <p class="text-muted font-light px-3" style="font-size: 0.7rem; height: 75px; overflow:hidden;">{{coupon.description}}</p>
            </div>
            
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="couponModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                
                <img :src="selectedCoupon.image" alt="">
                <div class="modal-body">
                    <div class="row">

                        <div class="col-md-12">
                            <h3 class="font-light">{{selectedCoupon.name}}</h3>
                        </div>
                        <div class="col-md-12">
                            <p class="text-muted font-light">
                                {{selectedCoupon.description}}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <button type="button" class="btn btn-info btn-round m-2" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>




<?php include('./includes/footer-includes.php'); ?>
<!-- Load js used in this page -->
<script type="module" src="./assets/js/scripts/benefits.js"></script>
<?php include('./includes/footer.php'); ?>