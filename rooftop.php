<?php include('./includes/constants.php'); ?>
<!-- Constantes de la aplicación -->
<?php include('./includes/header.php'); ?>

<div id="app" v-cloak>
    <!-- Slider -->
    <div class="container-fluid d-flex align-items-center justify-content-center flex-column" style="background-image: url('./assets/img/rooftop/header.png'); height: 550px; background-size: cover; background-position: center;">
        <h4 class="text-white m-0 special-font-title font-italic">Take it to the</h4>
        <h1 class=" text-white special-font-title text-uppercase font-weight-bold" style="font-size: 4rem;">Top</h1>
    </div>
    <div class="container-fluid my-5 left-image-right-content">
        <div class="row my-4">
            <div class="col-md-6 px-0">
                <img src="./assets/img/rooftop/first-section.png" alt="">
            </div>
            <div class="col-md-6 d-flex flex-column align-items-center justify-content-center mt-sm-5">
                <div class="content-side">
                    <h2 class="font-light">La terraza más TOP de toda la ciudad de Guatemala</h2>
                    <p class="text-muted font-light">Rooftop es un lifestyle center único en diseño, arquitectura y experiencia. Abarca una variedad de conceptos exclusivos en un ambiente de primer nivel al aire libre. De día y de noche, todo en un solo lugar.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row rooftop-categories">


            <div class="col-md-3 text-center directory-home-item" :class="{'directory-item-active': selectedCategoryIndex === 0}" v-on:click="selectCategory(0, rooftopData.categories[0])">
                <img src="./assets/img/home/home-page-benefits-placeholder.png" alt="">
                <p class="font-light">Concepto de postres</p>
            </div>
            <div class="col-md-3 text-center directory-home-item" :class="{'directory-item-active': selectedCategoryIndex === 1}" v-on:click="selectCategory(1, rooftopData.categories[1])">
                <img src="./assets/img/home/home-page-benefits-placeholder.png" alt="">
                <p class="font-light">Salón y spa</p>
            </div>
            <div class="col-md-3 text-center directory-home-item" :class="{'directory-item-active': selectedCategoryIndex === 2}" v-on:click="selectCategory(2, rooftopData.categories[2])">
                <img src="./assets/img/home/home-page-benefits-placeholder.png" alt="">
                <p class="font-light">Academias</p>
            </div>
            <div class="col-md-3 text-center directory-home-item" :class="{'directory-item-active': selectedCategoryIndex === 3}" v-on:click="selectCategory(3, rooftopData.categories[3])">
                <img src="./assets/img/home/home-page-benefits-placeholder.png" alt="">
                <p class="font-light">Restaurante de tenedor</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="height: 410px;">
                <div class="owl-carousel owl-theme">
                    <div v-for="slide in selectedCategory.category_items">
                        <div class="text-center directory-home-item" :style="{'background-image': 'url('+ slide.img + ')'}" style="height: 400px; width: 100%; padding:0;">
                            <p class="text-white mb-0 font-light" style="font-size: 1.2rem;">{{slide.name}}</p>
                            <p class="text-white special-font-title" style="font-size: 0.8rem;">Ver más</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mt-5 text-center">
                <a class="btn btn-round btn-info" style="min-width: 150px;" href="directorio.php">Ir al directorio</a>
            </div>

        </div>
    </div>

    <div class="container p-0 mt-5">
        <div class="row">
            <div class="col-md-12 p-0">
                <img src="./assets/img/rooftop/info-section.png" alt="Reserva ya">
                <div class="row" style="position:absolute; bottom: 5px; right: 15px; width: 100%; padding:15px 25px 0 25px;">
                    <div class="col-md-6 text-white">
                        <h3 class="special-font-title mb-1">Eventos TOP</h3>
                        <p class="font-light" style="font-size: 0.8rem;">Un espacio destinado para obras, exposiciones, conciertos, shows y eventos privados. Con acabados espectaculares, 800m² y ubicación estratégica, Sala Expo es el espacio perfecto para tu evento.</p>
                    </div>
                    <div class="col-md-6 text-right">
                        <button class="btn btn-info btn-round" style="min-width: 150px;">Enviar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<?php include('./includes/footer-includes.php'); ?>
<!-- Load js used in this page -->
<script type="module" src="./assets/js/scripts/rooftop.js"></script>
<?php include('./includes/footer.php'); ?>